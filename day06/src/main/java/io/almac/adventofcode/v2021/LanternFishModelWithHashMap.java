package io.almac.adventofcode.v2021;

import java.util.HashMap;
import java.util.List;

public class LanternFishModelWithHashMap {

    private final HashMap<Integer, Long> simplifiedModel = new HashMap<>();

    private static final int ITERATIONS = 256;

    public long simulateFishGrowth(final List<Integer> input) {
        return simulateFishGrowth(input, ITERATIONS);
    }

    public long simulateFishGrowth(final List<Integer> input, final int targetIterations) {
        for (var i = 0; i < 9; i++) {
            simplifiedModel.putIfAbsent(i, 0L);
        }
        for (final var fish : input) {
            simplifiedModel.computeIfPresent(fish, (key, counter) -> ++counter);
        }
        for (var i = 0; i < targetIterations; i++) {
            System.out.println("Iteration > " + i);

            final var fishToRespawn = simplifiedModel.get(0);
            final var oneTickFish = simplifiedModel.get(1);
            final var twoTickFish = simplifiedModel.get(2);
            final var threeTickFish = simplifiedModel.get(3);
            final var fourTickFish = simplifiedModel.get(4);
            final var fiveTickFish = simplifiedModel.get(5);
            final var sixTickFish = simplifiedModel.get(6);
            final var sevenTickFish = simplifiedModel.get(7);
            final var eightTickFish = simplifiedModel.get(8);

            simplifiedModel.put(0, oneTickFish);
            simplifiedModel.put(1, twoTickFish);
            simplifiedModel.put(2, threeTickFish);
            simplifiedModel.put(3, fourTickFish);
            simplifiedModel.put(4, fiveTickFish);
            simplifiedModel.put(5, sixTickFish);
            simplifiedModel.put(6, sevenTickFish + fishToRespawn);
            simplifiedModel.put(7, eightTickFish);
            simplifiedModel.put(8, fishToRespawn);
        }
        return simplifiedModel.values()
                .stream()
                .reduce(0L, Long::sum);
    }

}

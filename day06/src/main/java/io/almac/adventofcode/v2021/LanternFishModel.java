package io.almac.adventofcode.v2021;

import java.util.ArrayList;
import java.util.List;

public class LanternFishModel {

    private static final int RESET_TO = 6;
    private static final int NEW_FISH_TIMER = 8;
    private static final int ITERATIONS = 80;
    private static final int AGE_TO_SPAWN_NEW_TIMER = 0;

    public int simulateFishGrowth(final List<LanternFish> input) {
        return simulateFishGrowth(input, ITERATIONS);
    }

    public int simulateFishGrowth(final List<LanternFish> input, final int targetIterations) {
        final var lanternFishList = new ArrayList<>(input);
        for (var i = 0; i < targetIterations; i++) {
            final var newFishes = new ArrayList<LanternFish>();
            for (final var lanternFish : lanternFishList) {
                if (lanternFish.getAge() == AGE_TO_SPAWN_NEW_TIMER) {
                    newFishes.add(spawnNewFish(lanternFish, NEW_FISH_TIMER));
                }
                if (lanternFish.getAge() == 0) {
                    lanternFish.resetAge(RESET_TO);
                } else {
                    lanternFish.decreaseAge();
                }
            }
            lanternFishList.addAll(newFishes);
        }
        return lanternFishList.size();
    }

    public LanternFish spawnNewFish(LanternFish parentFish, int age) {
        return LanternFish.builder()
                .parent(parentFish)
                .age(age)
                .build();
    }

}

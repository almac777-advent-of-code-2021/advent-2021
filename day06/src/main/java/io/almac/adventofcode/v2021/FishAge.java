package io.almac.adventofcode.v2021;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class FishAge {
    int age;
}

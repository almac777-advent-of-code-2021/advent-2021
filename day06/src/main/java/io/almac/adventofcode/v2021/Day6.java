package io.almac.adventofcode.v2021;

import io.almac.adventofcode.v2021.utils.FileInputReader;

public class Day6 {

    public static void main(String[] args) {
        part1();
        part2();
    }

    private static void part2() {
        final var lanternFishModelWithHashMap = new LanternFishModelWithHashMap();
        final var inputReader = new FileInputReader();
        final var input = inputReader.readLinesAsInteger("day6/input");
        final var res = lanternFishModelWithHashMap.simulateFishGrowth(input, 256);
        System.out.println("Answer 2 > " + res);
    }

    private static void part1() {
        final var lanternFishModelWithHashMap = new LanternFishModelWithHashMap();
        final var inputReader = new FileInputReader();
        final var input = inputReader.readLinesAsInteger("day6/input");
        final var res = lanternFishModelWithHashMap.simulateFishGrowth(input, 80);
        System.out.println("Answer 1 > " + res);
    }

}

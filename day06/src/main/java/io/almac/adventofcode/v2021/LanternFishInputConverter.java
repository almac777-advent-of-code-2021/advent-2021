package io.almac.adventofcode.v2021;

import java.util.ArrayList;
import java.util.List;

public class LanternFishInputConverter {

    public List<LanternFish> convertInput(List<Integer> input) {
        return input.stream()
                .map(i -> LanternFish.builder().age(i).build())
                .toList();
    }

    public List<LanternFishAndDescendants> convertInputWithDescendants(List<Integer> input) {
        return input.stream()
                .map(i -> LanternFishAndDescendants.builder()
                        .age(i)
                        .descendants(new ArrayList<>())
                        .build()
                ).toList();
    }

}

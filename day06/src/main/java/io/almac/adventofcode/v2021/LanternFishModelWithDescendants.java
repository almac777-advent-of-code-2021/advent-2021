package io.almac.adventofcode.v2021;

import java.util.List;

public class LanternFishModelWithDescendants {

    private static final int RESET_TO = 6;
    private static final int NEW_FISH_TIMER = 8;
    private static final int ITERATIONS = 256;
    private static final int AGE_TO_SPAWN_NEW_TIMER = 0;

    public long simulateFishGrowth(final List<LanternFishAndDescendants> input) {
        return simulateFishGrowth(input, ITERATIONS);
    }

    public long simulateFishGrowth(final List<LanternFishAndDescendants> input, final int targetIterations) {
        for (var i = 0; i < targetIterations; i++) {
            System.out.println("Iteration > " + i);
            for (final var lanternFish : input) {
                lanternFish.tick();
            }
        }
        return input.stream()
                .reduce(
                        (long) input.size(),
                        (sum, lanternFishAndDescendants) -> sum += lanternFishAndDescendants.calculateDescendants(0L),
                        Long::sum
                );
    }

}

package io.almac.adventofcode.v2021;

import lombok.Builder;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Builder
public class LanternFishAndDescendants {

    private static final int RESET_TO = 6;
    private static final int NEW_FISH_TIMER = 8;
    private static final int ITERATIONS = 256;
    private static final int AGE_TO_SPAWN_NEW_TIMER = 0;

    private int age;
    private List<LanternFishAndDescendants> descendants;

    private void decreaseAge() {
        age--;
    }

    private void resetAge() {
        age = RESET_TO;
    }

    private void spawnNewFish() {
        descendants.add(LanternFishAndDescendants.builder()
                .age(NEW_FISH_TIMER)
                .descendants(new ArrayList<>())
                .build()
        );
    }

    public void tick() {
        var isSpawnFish = age == AGE_TO_SPAWN_NEW_TIMER;
        if (age == 0) {
            resetAge();
        } else {
            decreaseAge();
        }
        for (final var descendant : descendants) {
            descendant.tick();
        }
        if (isSpawnFish) {
            spawnNewFish();
        }
    }

    public long calculateDescendants(long currentResult) {
        currentResult += descendants.size();
        for (final var descendant : descendants) {
            currentResult += descendant.calculateDescendants(0L);
        }
        return currentResult;
    }
}

package io.almac.adventofcode.v2021;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class LanternFish {
    private LanternFish parent;
    private int age;

    void decreaseAge() {
        age--;
    }

    void resetAge(int resetAge) {
        age = resetAge;
    }
}

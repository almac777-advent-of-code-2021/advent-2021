package io.almac.adventofcode.v2021;

import io.almac.adventofcode.v2021.utils.FileInputReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class LanternFishModelTest {

    private LanternFishModel lanternFishModel;

    @BeforeEach
    void setup() {
        lanternFishModel = new LanternFishModel();
    }

    @Test
    void simulateFishes() {
        final var inputReader = new FileInputReader();
        final var converter = new LanternFishInputConverter();
        final var input = inputReader.readLinesAsInteger("day6/input");
        final var startPopulation = converter.convertInput(input);
        final var res = lanternFishModel.simulateFishGrowth(startPopulation, 18);
        assertThat(res).isEqualTo(26);
    }

    @Test
    void simulateFishesFullIteration() {
        final var inputReader = new FileInputReader();
        final var converter = new LanternFishInputConverter();
        final var input = inputReader.readLinesAsInteger("day6/input");
        final var startPopulation = converter.convertInput(input);
        final var res = lanternFishModel.simulateFishGrowth(startPopulation);
        assertThat(res).isEqualTo(5934);
    }


    @Test
    void simulateFishesExtendedIteration_smallAmountOfIterations() {
        final var lanternFishWithDescendantsModel = new LanternFishModelWithDescendants();
        final var inputReader = new FileInputReader();
        final var converter = new LanternFishInputConverter();
        final var input = inputReader.readLinesAsInteger("day6/input");
        final var startPopulation = converter.convertInputWithDescendants(input);
        final var res = lanternFishWithDescendantsModel.simulateFishGrowth(startPopulation, 18);
        assertThat(res).isEqualTo(26);
    }

    @Test
    void simulateFishesExtendedIteration_mediumAmountOfIterations() {
        final var lanternFishWithDescendantsModel = new LanternFishModelWithDescendants();
        final var inputReader = new FileInputReader();
        final var converter = new LanternFishInputConverter();
        final var input = inputReader.readLinesAsInteger("day6/input");
        final var startPopulation = converter.convertInputWithDescendants(input);
        final var res = lanternFishWithDescendantsModel.simulateFishGrowth(startPopulation, 80);
        assertThat(res).isEqualTo(5_934);
    }

    @Test
    @Disabled
    void simulateFishesExtendedIteration_largeAmountOfIterations() {
        final var lanternFishWithDescendantsModel = new LanternFishModelWithDescendants();
        final var inputReader = new FileInputReader();
        final var converter = new LanternFishInputConverter();
        final var input = inputReader.readLinesAsInteger("day6/input");
        final var startPopulation = converter.convertInputWithDescendants(input);
        final var res = lanternFishWithDescendantsModel.simulateFishGrowth(startPopulation, 256);
        assertThat(res).isEqualTo(26_984_457_539L);
    }
}
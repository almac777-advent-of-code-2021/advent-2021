package io.almac.adventofcode.v2021;

import io.almac.adventofcode.v2021.utils.FileInputReader;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class LanternFishModelWithHashMapTest {

    @Test
    void simulateFishesExtendedIteration_noIterations() {
        final var lanternFishModelWithHashMap = new LanternFishModelWithHashMap();
        final var inputReader = new FileInputReader();
        final var input = inputReader.readLinesAsInteger("day6/input");
        final var res = lanternFishModelWithHashMap.simulateFishGrowth(input, 0);
        assertThat(res).isEqualTo(5);
    }

    @Test
    void simulateFishesExtendedIteration_smallAmountOfIterations() {
        final var lanternFishModelWithHashMap = new LanternFishModelWithHashMap();
        final var inputReader = new FileInputReader();
        final var input = inputReader.readLinesAsInteger("day6/input");
        final var res = lanternFishModelWithHashMap.simulateFishGrowth(input, 18);
        assertThat(res).isEqualTo(26);
    }

    @Test
    void simulateFishGrowth_mediumInput() {
        final var lanternFishModelWithHashMap = new LanternFishModelWithHashMap();
        final var inputReader = new FileInputReader();
        final var input = inputReader.readLinesAsInteger("day6/input");
        final var res = lanternFishModelWithHashMap.simulateFishGrowth(input, 80);
        assertThat(res).isEqualTo(5_934);
    }

    @Test
    void simulateFishGrowth_largeInput() {
        final var lanternFishModelWithHashMap = new LanternFishModelWithHashMap();
        final var inputReader = new FileInputReader();
        final var input = inputReader.readLinesAsInteger("day6/input");
        final var res = lanternFishModelWithHashMap.simulateFishGrowth(input, 256);
        assertThat(res).isEqualTo(26_984_457_539L);
    }
}
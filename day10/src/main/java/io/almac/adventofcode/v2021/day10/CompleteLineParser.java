package io.almac.adventofcode.v2021.day10;

import java.util.LinkedList;
import java.util.List;

public class CompleteLineParser {

    public CompletionResult complete(List<OpenCloseMarker> markers) {
        final var invertedRes = new LinkedList<OpenCloseMarker>();
        for (final var currentMarker : markers) {
            if (currentMarker.isOpen()) {
                final var close = OpenCloseMarker.builder()
                        .character(currentMarker.expectedClosingTag())
                        .open(currentMarker)
                        .close(null)
                        .openingIndex(currentMarker.openingIndex)
                        .closingIndex(markers.size() + invertedRes.size() + 1)
                        .build();
                currentMarker.setClose(close);
                invertedRes.add(close);
            }
        }
        final var res = invert(invertedRes);
        markers.addAll(res);
        return CompletionResult.of(res);
    }

    private List<OpenCloseMarker> invert(List<OpenCloseMarker> invertedRes) {
        final List<OpenCloseMarker> inverted = new LinkedList<OpenCloseMarker>();
        for (var i = invertedRes.size() - 1; i >= 0; i--) {
            inverted.add(invertedRes.get(i));
        }
        return inverted;
    }

}

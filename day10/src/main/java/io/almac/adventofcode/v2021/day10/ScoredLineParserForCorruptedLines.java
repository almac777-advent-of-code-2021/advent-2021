package io.almac.adventofcode.v2021.day10;

import java.util.LinkedList;
import java.util.List;

public class ScoredLineParserForCorruptedLines {

    private SyntaxCheckResult syntaxCheckResult;

    public ScoredLineParserForCorruptedLines(SyntaxCheckResult syntaxCheckResult) {
        this.syntaxCheckResult = syntaxCheckResult;
    }

    public List<OpenCloseMarker> checkLine(String line) {
        final var openCloseMarkers = new LinkedList<OpenCloseMarker>();
        SyntaxCheck syntaxCheck = SyntaxCheck.of(openCloseMarkers);
        var isCorruptionDetected = false;
        for (var i = 0; i < line.length() && !isCorruptionDetected; i++) {
            final var c = line.charAt(i);
            final var oc = OpenCloseMarker.builder()
                    .character(c)
                    .openingIndex(i)
                    .closingIndex(0)
                    .open(null)
                    .close(null)
                    .build();

            openCloseMarkers.add(oc);
            if (isClosingTag(oc)) {
                final var corrupted = syntaxCheck.findFirstOpenOpeningTag(openingTagFor(c));
                if (corrupted != null) {
                    corrupted.setClose(oc);
                    oc.setOpen(corrupted);
                    addCorruptedMarkerToSyntaxCheck(corrupted);
                    isCorruptionDetected = true;
                } else {
                    final var openingTag = syntaxCheck.findLastMarkerForCharacterThatIsOpen(openingTagFor(c));
                    if (openingTag != null) {
                        oc.setOpen(openingTag);
                        openingTag.setClosingIndex(i);
                        openingTag.setClose(oc);
                    }
                }
            }
        }
        return isCorruptionDetected ? null : openCloseMarkers;
    }

    private Character openingTagFor(char c) {
        switch (c) {
            case ')' -> {
                return '(';
            }
            case ']' -> {
                return '[';
            }
            case '}' -> {
                return '{';
            }
            case '>' -> {
                return '<';
            }
        }
        throw new RuntimeException("Invalid application state, c is already an opening tag => " + c);
    }

    private void addCorruptedMarkerToSyntaxCheck(OpenCloseMarker corruptedMarker) {
        syntaxCheckResult.getCorruptedCharacters().add(corruptedMarker);
    }

    private boolean isClosingTag(OpenCloseMarker oc) {
        return oc.getCharacter() == ')'
                || oc.getCharacter() == ']'
                || oc.getCharacter() == '}'
                || oc.getCharacter() == '>';
    }

}

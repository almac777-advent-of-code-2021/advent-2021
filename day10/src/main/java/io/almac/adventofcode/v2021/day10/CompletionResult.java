package io.almac.adventofcode.v2021.day10;

import lombok.Value;

import java.util.List;

@Value(staticConstructor = "of")
public class CompletionResult {
    List<OpenCloseMarker> completed;
}

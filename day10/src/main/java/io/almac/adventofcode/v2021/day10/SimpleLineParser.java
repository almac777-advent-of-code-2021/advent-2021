package io.almac.adventofcode.v2021.day10;

import lombok.Getter;

@Getter
public class SimpleLineParser {

    public boolean isValid(String s) {
        var paranthesis = 0;
        var brackets = 0;
        var curlies = 0;
        var tags = 0;
        for (var i = 0; i < s.length(); i++) {
            switch (s.charAt(i)) {
                case '(' -> paranthesis++;
                case ')' -> paranthesis--;
                case '[' -> brackets++;
                case ']' -> brackets--;
                case '{' -> curlies++;
                case '}' -> curlies--;
                case '<' -> tags++;
                case '>' -> tags--;
            }
        }
        return paranthesis == 0 && brackets == 0 && curlies == 0 && tags == 0;
    }

}

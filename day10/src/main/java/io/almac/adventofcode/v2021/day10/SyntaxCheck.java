package io.almac.adventofcode.v2021.day10;

import lombok.Value;

import java.util.List;

@Value(staticConstructor = "of")
public class SyntaxCheck {

    List<OpenCloseMarker> openCloseMarkers;

    public OpenCloseMarker findLastMarkerForCharacterThatIsOpen(Character closing) {
        OpenCloseMarker lastOpenClose = null;
        for (final var openCloseMarker : openCloseMarkers) {
            if (openCloseMarker.getCharacter().equals(closing) && openCloseMarker.isOpen()) {
                lastOpenClose = openCloseMarker;
            }
        }
        return lastOpenClose;
    }

    public OpenCloseMarker findFirstOpenOpeningTag(Character openingTagFor) {
        for (var i = openCloseMarkers.size() - 1; i >= 0; i--) {
            final var openCloseMarker = openCloseMarkers.get(i);
            if (openCloseMarker.isOpeningTag() && openCloseMarker.isOpen() && openCloseMarker.getCharacter() == openingTagFor) {
                return null;
            }
            if (openCloseMarker.isOpeningTag() && openCloseMarker.isOpen() && openCloseMarker.getCharacter() != openingTagFor) {
                return openCloseMarker;
            }
        }
        return null;
    }
}

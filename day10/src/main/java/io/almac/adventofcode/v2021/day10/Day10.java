package io.almac.adventofcode.v2021.day10;

import io.almac.adventofcode.v2021.utils.FileInputReader;

import java.util.LinkedList;
import java.util.List;

public class Day10 {

    public static void main(String[] args) {
        final var uncorruptedLines = part1();
        part2(uncorruptedLines);
    }

    private static List<List<OpenCloseMarker>> part1() {
        final var inputReader = new FileInputReader();
        final var lines = inputReader.readLines("day10/input");
        final var simpleLineChecker = new SimpleLineParser();
        final var simpleScorer = new SimpleScorer();
        final var syntaxCheckResult = SyntaxCheckResult.of(new LinkedList<>(), new LinkedList<>());
        final var scoredLineParser = new ScoredLineParserForCorruptedLines(syntaxCheckResult);

        final List<List<OpenCloseMarker>> uncorruptedLines = new LinkedList<List<OpenCloseMarker>>();
        for (var line : lines) {
            if (! simpleLineChecker.isValid(line)) {
                final var res = scoredLineParser.checkLine(line);
                if (res != null) {
                    uncorruptedLines.add(res);
                }
            }
        }
        System.out.println("Answer 1 > " + simpleScorer.tallyScore(syntaxCheckResult));
        return uncorruptedLines;
    }

    private static void part2(final List<List<OpenCloseMarker>> uncorruptedLines) {
        final var simpleScorer = new SimpleScorer();
        final var completeLineParser = new CompleteLineParser();
        final var completionCheckResult = SyntaxCheckResult.of(new LinkedList<>(), new LinkedList<>());

        for (var line : uncorruptedLines) {
            final var res = completeLineParser.complete(line);
            completionCheckResult.getCompletionResults().add(res);
        }

        System.out.println("Answer 2 > " + simpleScorer.tallyScoreForCompletion(completionCheckResult));
    }

}

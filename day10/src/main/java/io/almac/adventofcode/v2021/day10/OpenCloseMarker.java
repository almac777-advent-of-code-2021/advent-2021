package io.almac.adventofcode.v2021.day10;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class OpenCloseMarker {

    int openingIndex;
    int closingIndex;

    Character character;

    OpenCloseMarker open;
    OpenCloseMarker close;

    boolean isOpeningTag() {
        return character == '('
                || character == '['
                || character == '{'
                || character == '<';
    }

    Character expectedClosingTag() {
        switch (character) {
            case '(' -> { return ')'; }
            case '[' -> { return ']'; }
            case '{' -> { return '}'; }
            case '<' -> { return '>'; }
        }
        return null;
    }

    boolean isOpen() {
        return isOpeningTag() && close == null;
    }

    boolean isClosingDespiteNoOpenTagExisting() {
        return close == this;
    }

}

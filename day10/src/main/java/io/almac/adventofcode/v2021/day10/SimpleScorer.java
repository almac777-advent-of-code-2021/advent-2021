package io.almac.adventofcode.v2021.day10;

import java.util.*;

public class SimpleScorer {

    private static final int SCORE_FOR_PARENTHESES = 3;
    private static final int SCORE_FOR_BRACKETS = 57;
    private static final int SCORE_FOR_CURLY = 1197;
    private static final int SCORE_FOR_TAG = 25137;

    private static final char PARENTHESES = ')';
    private static final char BRACKET = ']';
    private static final char CURLY = '}';
    private static final char TAG = '>';

    private final Map<Character, Integer> scoringMapForCorruptedElements = new HashMap<>();
    private final Map<Character, Integer> scoringMapForCompletedElements = new HashMap<>();

    public SimpleScorer() {
        scoringMapForCorruptedElements.put(PARENTHESES, SCORE_FOR_PARENTHESES);
        scoringMapForCorruptedElements.put(BRACKET, SCORE_FOR_BRACKETS);
        scoringMapForCorruptedElements.put(CURLY, SCORE_FOR_CURLY);
        scoringMapForCorruptedElements.put(TAG, SCORE_FOR_TAG);

        scoringMapForCompletedElements.put(PARENTHESES, 1);
        scoringMapForCompletedElements.put(BRACKET, 2);
        scoringMapForCompletedElements.put(CURLY, 3);
        scoringMapForCompletedElements.put(TAG, 4);
    }

    public int tallyScore(SyntaxCheckResult missedCharacter) {
        var sum = 0;
        for (OpenCloseMarker c : missedCharacter.getCorruptedCharacters()) {
            sum += Optional.ofNullable(scoringMapForCorruptedElements.get(c.getClose().getCharacter())).orElse(0);
        }
        return sum;
    }

    public long tallyScoreForCompletion(SyntaxCheckResult syntaxCheckResult) {
        List<Long> sums = new LinkedList<>();
        for (var completionResults : syntaxCheckResult.getCompletionResults()) {
            var partialSum = 0L;
            for (var completionResult : completionResults.getCompleted()) {
                partialSum = partialSum * 5 + scoringMapForCompletedElements.get(completionResult.getCharacter());
            }
            sums.add(partialSum);
        }
        final var sortedList = sums.stream()
                .sorted()
                .toList();
        return sortedList.get(sums.size() / 2);
    }

}

package io.almac.adventofcode.v2021.day10;

import io.almac.adventofcode.v2021.utils.FileInputReader;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class Part1Test {

    @Test
    void line() {
        final var inputReader = new FileInputReader();
        final var lines = inputReader.readLines("day10/input");
        final var simpleLineChecker = new SimpleLineParser();
        final var simpleScorer = new SimpleScorer();
        final var syntaxCheckResult = SyntaxCheckResult.of(new LinkedList<>(), new LinkedList<>());
        final var scoredLineParser = new ScoredLineParserForCorruptedLines(syntaxCheckResult);
        for (var line : lines) {
            if (! simpleLineChecker.isValid(line)) {
                scoredLineParser.checkLine(line);
            }
        }
        assertThat(simpleScorer.tallyScore(syntaxCheckResult)).isEqualTo(26_397);
    }
}
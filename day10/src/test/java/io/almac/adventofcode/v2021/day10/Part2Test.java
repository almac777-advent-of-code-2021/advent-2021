package io.almac.adventofcode.v2021.day10;

import io.almac.adventofcode.v2021.utils.FileInputReader;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class Part2Test {

    @Test
    void line() {
        final var inputReader = new FileInputReader();
        final var lines = inputReader.readLines("day10/input");
        final var simpleLineChecker = new SimpleLineParser();
        final var simpleScorer = new SimpleScorer();
        final var syntaxCheckResult = SyntaxCheckResult.of(new LinkedList<>(), new LinkedList<>());
        final var scoredLineParser = new ScoredLineParserForCorruptedLines(syntaxCheckResult);
        final var uncorruptedLines = new LinkedList<List<OpenCloseMarker>>();
        for (var line : lines) {
            if (! simpleLineChecker.isValid(line)) {
                final var res = scoredLineParser.checkLine(line);
                if (res != null) {
                    uncorruptedLines.add(res);
                }
            }
        }
        assertThat(simpleScorer.tallyScore(syntaxCheckResult)).isEqualTo(26_397);
        assertThat(uncorruptedLines).hasSize(5);

        final var completeLineParser = new CompleteLineParser();
        final var completedLines = new LinkedList<List<OpenCloseMarker>>();
        final var completionCheckResult = SyntaxCheckResult.of(new LinkedList<>(), new LinkedList<>());
        for (var line : uncorruptedLines) {
            final var res = completeLineParser.complete(line);
            completionCheckResult.getCompletionResults().add(res);
            completedLines.add(res.getCompleted());
        }
        // 36 => "}}]])})])}>]})}}>}>))))]]}}]}]}>])}>".length()
        final var expectedLength = 36;
        final var totalLength = completedLines.stream()
                .map(List::size)
                .reduce(0, Integer::sum);
        assertThat(totalLength).isEqualTo(expectedLength);

        assertThat(simpleScorer.tallyScoreForCompletion(completionCheckResult)).isEqualTo(288_957);
    }
}
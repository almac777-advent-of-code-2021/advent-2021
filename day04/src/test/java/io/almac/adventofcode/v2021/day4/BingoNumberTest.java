package io.almac.adventofcode.v2021.day4;

import io.almac.adventofcode.v2021.utils.FileInputReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.fail;

class BingoNumberTest {

    private BingoBoardReader bingoBoardReader = new BingoBoardReader();
    private FileInputReader numberReader = new FileInputReader();

    @BeforeEach
    void setup() {
        bingoBoardReader = new BingoBoardReader();
        numberReader = new FileInputReader();
    }

    @Test
    void initiate() {
        final var numbers = numberReader.readLinesAsInteger("day4/test-input");
        final var boards = bingoBoardReader.readBingoTableFrom("day4/test-boards");
        var assertCalled = false;

        for (var number : numbers) {
            System.out.println("Drawing: " + number);
            for (var board : boards) {
                var bingoData = board.isWinner(number);
                if (bingoData.isPresent()) {
                    assertThat(bingoData.get()).isEqualTo(4512);
                    assertCalled = true;
                    break;
                }
            }
            if (assertCalled) {
                break;
            }
        }

        if (!assertCalled) {
            fail("- No assert called -");
        }
    }

}
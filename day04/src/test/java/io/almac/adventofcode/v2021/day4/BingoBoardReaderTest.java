package io.almac.adventofcode.v2021.day4;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class BingoBoardReaderTest {

    private BingoBoardReader bingoBoardReader;

    @BeforeEach
    void setup() {
        bingoBoardReader = new BingoBoardReader();
    }

    @Test
    void readBingoTableFrom() {
        final var res = bingoBoardReader.readBingoTableFrom("day4/test-board");
        assertThat(res)
                .isNotNull()
                .isNotEmpty()
                .hasSize(1);

        final var board = res.get(0);

        // 0, 24, 7, 5, 19
        assertThat(board.isWinner(0)).isEmpty();
        assertThat(board.isWinner(24)).isEmpty();
        assertThat(board.isWinner(7)).isEmpty();
        assertThat(board.isWinner(5)).isEmpty();
        var isWinnerRes = board.isWinner(19);
        assertThat(isWinnerRes).isNotEmpty();
    }
}
package io.almac.adventofcode.v2021.day4;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class BingoNumberFactoryTest {

    private BingoNumberFactory bingoNumberFactory;

    @BeforeEach
    void setup() {
        bingoNumberFactory = new BingoNumberFactory();
    }

    @Test
    void testBingoDataGeneration() {
        BingoData[][] arr = new BingoData[5][5];
        for (var i = 0; i < 5; i++) {
            for (var j = 0; j < 5; j++) {
                var number = (i * j) + j;
                var bingoData = bingoNumberFactory.build(number);
                assertThat(bingoData)
                        .hasFieldOrPropertyWithValue("rowIndex", i)
                        .hasFieldOrPropertyWithValue("colIndex", j);
                assertThat(bingoData.getRow().allMarked())
                        .isFalse();
                assertThat(bingoData.getColumn().allMarked())
                        .isFalse();
                assertThat(bingoData.getNumber())
                        .hasFieldOrPropertyWithValue("number", number)
                        .hasFieldOrPropertyWithValue("marked", false);

                arr[i][j] = bingoData;
            }
        }

        assertThat(arr[0][0].getRow()).isSameAs(arr[0][4].getRow());
        assertThat(arr[0][0].getColumn()).isSameAs(arr[4][0].getColumn());

        arr[0][0].getNumber().mark();
        arr[0][1].getNumber().mark();
        arr[0][2].getNumber().mark();
        arr[0][3].getNumber().mark();
        arr[0][4].getNumber().mark();

        assertThat(arr[0][0].getRow().allMarked()).isTrue();

        assertThat(arr[0][0].getColumn().allMarked()).isFalse();
        assertThat(arr[1][0].getRow().allMarked()).isFalse();
        assertThat(arr[1][1].getRow().allMarked()).isFalse();
    }

}
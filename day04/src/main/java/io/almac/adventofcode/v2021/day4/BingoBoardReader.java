package io.almac.adventofcode.v2021.day4;

import io.almac.adventofcode.v2021.utils.FileInputReader;

import java.util.LinkedList;
import java.util.List;

public class BingoBoardReader {

    private final FileInputReader fileInputReader = new FileInputReader();

    public List<BingoTable> readBingoTableFrom(String location) {
        final var results = new LinkedList<BingoTable>();
        final var lines = fileInputReader.readLines(location);
        var bingoTable = new BingoTable();
        for (final var line : lines) {
            if (line.isBlank() || line.isEmpty()) {
                results.add(bingoTable);
                bingoTable = new BingoTable();
            }
            handleLine(line, bingoTable);
        }
        results.add(bingoTable);
        return results;
    }

    public void handleLine(String line, BingoTable bingoTable) {
        final var tokens = line.split(" ");
        for (final var token : tokens) {
            handleToken(token, bingoTable);
        }
    }

    public void handleToken(String token, BingoTable bingoTable) {
        if (token == null || token.isBlank()) {
            return;
        }

        bingoTable.addNumber(Integer.parseInt(token));
    }

}

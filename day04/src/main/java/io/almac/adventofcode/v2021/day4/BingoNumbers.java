package io.almac.adventofcode.v2021.day4;

import lombok.Value;

import java.util.ArrayList;
import java.util.List;

@Value
public class BingoNumbers {

    List<BingoNumber> numbers = new ArrayList<>();

    public void addNumber(BingoNumber number) {
        if (numbers.size() == 5) {
            throw new RuntimeException("This is already full");
        }
        numbers.add(number);
    }

    public boolean allMarked() {
        return numbers.stream().allMatch(BingoNumber::isMarked);
    }

}

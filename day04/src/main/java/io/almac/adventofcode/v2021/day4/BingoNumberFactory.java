package io.almac.adventofcode.v2021.day4;

import java.util.ArrayList;
import java.util.List;

public class BingoNumberFactory {

    private int numberCounter;

    private static final int NUMBER_OF_ROWS = 5;
    private static final int NUMBER_OF_COLS = 5;

    private List<BingoNumbers> rows = new ArrayList<>(5);
    private List<BingoNumbers> cols = new ArrayList<>(5);

    public BingoNumberFactory() {
        // five pre-filled
        rows.add(new BingoNumbers());
        rows.add(new BingoNumbers());
        rows.add(new BingoNumbers());
        rows.add(new BingoNumbers());
        rows.add(new BingoNumbers());

        // five pre-filled
        cols.add(new BingoNumbers());
        cols.add(new BingoNumbers());
        cols.add(new BingoNumbers());
        cols.add(new BingoNumbers());
        cols.add(new BingoNumbers());
    }

    public BingoData build(int number) {
        BingoNumber bingoNumber = BingoNumber.builder()
                .number(number)
                .marked(false)
                .build();

        final var rowIndex = getRow();
        final var colIndex = getCol();

        rows.get(getRow()).addNumber(bingoNumber);
        cols.get(getCol()).addNumber(bingoNumber);

        numberCounter++;

        return BingoData.builder()
                .value(number)
                .rowIndex(rowIndex)
                .colIndex(colIndex)
                .number(bingoNumber)
                .row(rows.get(rowIndex))
                .column(cols.get(colIndex))
                .build();
    }

    private int getRow() {
        return numberCounter / NUMBER_OF_ROWS;
    }

    private int getCol() {
        return numberCounter % NUMBER_OF_COLS;
    }
}

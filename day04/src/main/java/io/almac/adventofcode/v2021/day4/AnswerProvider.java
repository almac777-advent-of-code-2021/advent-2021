package io.almac.adventofcode.v2021.day4;

import java.util.List;

public class AnswerProvider {

    public static int provideAnswer(List<BingoTable> boards, List<Integer> numbers, boolean findFirst) {
        var answer = 0;
        var isAnswerReached = false;
        for (int i = 0; i < numbers.size() && !isAnswerReached; i++) {
            var drawnNumber = numbers.get(i);
            for (int j = 0; j < boards.size() && !isAnswerReached; j++) {
                var board = boards.get(j);
                if (!findFirst && board.isWon()) {
                    continue;
                }
                var res = board.isWinner(drawnNumber);
                if (res.isPresent()) {
                    if (findFirst) {
                        isAnswerReached = true;
                    }
                    answer = res.get();
                    System.out.println("Winner > " + answer);
                }
            }
        }
        return answer;
    }
}

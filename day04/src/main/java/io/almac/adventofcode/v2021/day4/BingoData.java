package io.almac.adventofcode.v2021.day4;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class BingoData {
    int value;
    int rowIndex;
    int colIndex;
    BingoNumber number;
    BingoNumbers row;
    BingoNumbers column;
}

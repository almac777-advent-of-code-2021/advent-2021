package io.almac.adventofcode.v2021.day4;

import io.almac.adventofcode.v2021.utils.FileInputReader;

public class Day4 {

    public static void main(String[] args) {
        final var fileInputReader = new FileInputReader();
        final var boardsReader = new BingoBoardReader();

        final var numbers = fileInputReader.readLinesAsInteger("day4/bingo-input");
        final var boards = boardsReader.readBingoTableFrom("day4/bingo-boards");

        final var answer = AnswerProvider.provideAnswer(boards, numbers, true);
        final var worstAnswer = AnswerProvider.provideAnswer(boards, numbers, false);

        System.out.println("Answer: " + answer);
        System.out.println("Better winning strategy: " + worstAnswer);
    }

}

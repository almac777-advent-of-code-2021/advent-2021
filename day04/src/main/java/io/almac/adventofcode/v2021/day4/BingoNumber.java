package io.almac.adventofcode.v2021.day4;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class BingoNumber {
    private int number;
    private boolean marked;

    void mark() {
        marked = true;
    }
}

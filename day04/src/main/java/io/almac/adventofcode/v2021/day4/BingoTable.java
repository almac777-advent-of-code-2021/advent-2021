package io.almac.adventofcode.v2021.day4;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class BingoTable {

    private boolean isWon = false;
    private final Map<Integer, BingoData> tableToMark = new HashMap<>();
    private final BingoNumberFactory bingoNumberFactory = new BingoNumberFactory();

    public void addNumber(int number) {
        final var data = bingoNumberFactory.build(number);
        tableToMark.put(number, data);
    }

    public Optional<Integer> isWinner(int number) {
        if (! tableToMark.containsKey(number)) {
            return Optional.empty();
        }
        tableToMark.get(number).getNumber().mark();
        final var data = tableToMark.get(number);
        if (data.getColumn().allMarked() || data.getRow().allMarked()) {
            isWon = true;
            return Optional.of(calculateSumOfUnmarked() * number);
        }
        return Optional.empty();
    }

    public boolean isWon() {
        return this.isWon;
    }

    public int calculateSumOfUnmarked() {
        return tableToMark.values()
                .stream()
                .filter(data -> !data.getNumber().isMarked())
                .map(data -> data.getNumber().getNumber())
                .reduce(0, Integer::sum);
    }
}
package io.almac.adventofcode.v2021.day1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class PreciseSonarDataInterpreterTest {

    private PreciseSonarDataInterpreter preciseSonarDataInterpreter;

    @BeforeEach
    void setup() {
        this.preciseSonarDataInterpreter = new PreciseSonarDataInterpreter();
    }

    @Test
    void test() {
        final var data = List.of(
                199, //  A
                200, //  A B
                208, //  A B C
                210, //    B C D
                200, //  E   C D
                207, //  E F   D
                240, //  E F G
                269, //    F G H
                260, //      G H
                263  //        H
        );

        final var res = preciseSonarDataInterpreter.interpretData(data);

        assertThat(res).isEqualTo(5);
    }

}

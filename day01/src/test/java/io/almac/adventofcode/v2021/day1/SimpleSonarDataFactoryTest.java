package io.almac.adventofcode.v2021.day1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class SimpleSonarDataFactoryTest {

    private SimpleSonarDataFactory sonarDataFactory;

    @BeforeEach
    void setup() {
        this.sonarDataFactory = new SimpleSonarDataFactory();
    }

    @Test
    void getData() {
        final var expected = List.of(
                SimpleSonarData.builder()
                        .previousMeasurement(1)
                        .currentMeasurement(1)
                        .build(),
                SimpleSonarData.builder()
                        .previousMeasurement(1)
                        .currentMeasurement(2)
                        .build(),
                SimpleSonarData.builder()
                        .previousMeasurement(2)
                        .currentMeasurement(3)
                        .build(),
                SimpleSonarData.builder()
                        .previousMeasurement(3)
                        .currentMeasurement(4)
                        .build(),
                SimpleSonarData.builder()
                        .previousMeasurement(4)
                        .currentMeasurement(5)
                        .build(),
                SimpleSonarData.builder()
                        .previousMeasurement(5)
                        .currentMeasurement(-1)
                        .build()
        );
        final List<Integer> data = List.of(1, 2, 3, 4, 5);
        data.forEach(i -> sonarDataFactory.addData(i));
        final var sonarData = sonarDataFactory.getData();
        assertThat(sonarData)
                .isNotNull()
                .isNotEmpty()
                .hasSize(expected.size())
                .containsExactlyElementsOf(expected);
    }
}
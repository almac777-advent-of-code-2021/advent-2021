package io.almac.adventofcode.v2021.day1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class SimpleSonarInterpreterTest {

    private SimpleSonarInterpreter simpleSonarInterpreter;

    @BeforeEach
    void setup() {
        simpleSonarInterpreter = new SimpleSonarInterpreter();
    }

    @Test
    void interpretData() {
        final var testData = List.of(
                199,
                200,
                208,
                210,
                200,
                207,
                240,
                269,
                260,
                263
        );
        assertThat(simpleSonarInterpreter.interpretData(testData))
                .isEqualTo(7);
    }
}
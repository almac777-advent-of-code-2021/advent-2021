package io.almac.adventofcode.v2021.day1;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class PreciseSonarDataFactoryTest {

    private PreciseSonarDataFactory preciseSonarDataFactory;

    @BeforeEach
    void setup() {
        preciseSonarDataFactory = new PreciseSonarDataFactory();
    }

    @Test
    void correctSetup() {
        assertThat(preciseSonarDataFactory).isNotNull();
    }

    @Test
    void addData() {
        final var data = List.of(
                199, //  A
                200, //  A B
                208, //  A B C
                210, //    B C D
                200, //  E   C D
                207, //  E F   D
                240, //  E F G
                269, //    F G H
                260, //      G H
                263  //        H
        );

        final var expectedData = List.of(
                PreciseSonarData.builder()
                        .firstMeasurement(199)
                        .secondMeasurement(200)
                        .thirdMeasurement(208)
                        .build(),
                PreciseSonarData.builder()
                        .firstMeasurement(200)
                        .secondMeasurement(208)
                        .thirdMeasurement(210)
                        .build(),
                PreciseSonarData.builder()
                        .firstMeasurement(208)
                        .secondMeasurement(210)
                        .thirdMeasurement(200)
                        .build(),
                PreciseSonarData.builder()
                        .firstMeasurement(210)
                        .secondMeasurement(200)
                        .thirdMeasurement(207)
                        .build(),
                PreciseSonarData.builder()
                        .firstMeasurement(200)
                        .secondMeasurement(207)
                        .thirdMeasurement(240)
                        .build(),
                PreciseSonarData.builder()
                        .firstMeasurement(207)
                        .secondMeasurement(240)
                        .thirdMeasurement(269)
                        .build(),
                PreciseSonarData.builder()
                        .firstMeasurement(240)
                        .secondMeasurement(269)
                        .thirdMeasurement(260)
                        .build(),
                PreciseSonarData.builder()
                        .firstMeasurement(269)
                        .secondMeasurement(260)
                        .thirdMeasurement(263)
                        .build()
        );

        data.forEach(preciseSonarDataFactory::addData);
        assertThat(preciseSonarDataFactory.getCollector())
                .isNotNull()
                .isNotEmpty()
                .hasSize(expectedData.size())
                .containsExactlyElementsOf(expectedData);

    }
}
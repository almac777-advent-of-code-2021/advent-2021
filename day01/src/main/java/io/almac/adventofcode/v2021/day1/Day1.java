package io.almac.adventofcode.v2021.day1;

import io.almac.adventofcode.v2021.utils.FileInputReader;

public class Day1 {

    public static void main(String[] args) {
        final var sonarReader = new FileInputReader();
        final var data = sonarReader.readLinesAsInteger("day1/input");
        final var simpleSonarInterpreter = new SimpleSonarInterpreter();
        final var preciseSonarInterpreter = new PreciseSonarDataInterpreter();

        System.out.println("> Solution 1.1 -> " + simpleSonarInterpreter.interpretData(data));
        System.out.println("> Solution 1.2 -> " + preciseSonarInterpreter.interpretData(data));
    }

}

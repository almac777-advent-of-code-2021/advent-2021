package io.almac.adventofcode.v2021.day1;

import java.util.LinkedList;
import java.util.List;

public class SimpleSonarDataFactory {

    private final List<SimpleSonarData> collector = new LinkedList<>();

    private Integer firstElement = -1;
    private Integer secondElement = -1;

    private boolean firstRun = true;

    public void addData(Integer rawData) {
        if (firstRun) {
            handleFirstRun(rawData);
        } else {
            handleSubsequentRuns(rawData);
        }
    }

    public List<SimpleSonarData> getData() {
        flush();
        return collector;
    }

    private void handleFirstRun(Integer rawData) {
        firstElement = secondElement = rawData;
        firstRun = false;
        flush();
    }

    private void handleSubsequentRuns(Integer rawData) {
        if (firstElement < 0) {
            firstElement = rawData;
        } else {
            secondElement = rawData;
            flush();
        }
    }

    private void flush() {
        collector.add(SimpleSonarData.builder()
                .previousMeasurement(firstElement == null ? 0 : firstElement)
                .currentMeasurement(secondElement == null ? 0 : secondElement)
                .build());
        reset();
    }

    private void reset() {
        firstElement = secondElement;
        secondElement = -1;
    }
}

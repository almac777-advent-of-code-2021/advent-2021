package io.almac.adventofcode.v2021.day1;

import java.util.LinkedList;
import java.util.List;

public class PreciseSonarDataFactory {

    private final List<PreciseSonarData> collector = new LinkedList<>();

    private static class SonarWindow {
        int counter;

        int firstMeasurement = -1;
        int secondMeasurement = -1;
        int thirdMeasurement = -1;

        public SonarWindow(int start) {
            counter = start;
        }

        private void setMeasurement(int data) {
            switch (counter) {
                case 0 -> firstMeasurement = data;
                case 1 -> secondMeasurement = data;
                case 2 -> thirdMeasurement = data;
            }
            counter++;
        }

        public int getCounter() {
            return counter;
        }

        public PreciseSonarData toPreciseSonarData() {
            return PreciseSonarData.builder()
                    .firstMeasurement(firstMeasurement)
                    .secondMeasurement(secondMeasurement)
                    .thirdMeasurement(thirdMeasurement)
                    .build();
        }
    }


    private SonarWindow window1 = new SonarWindow(0);
    private SonarWindow window2 = new SonarWindow(-1);
    private SonarWindow window3 = new SonarWindow(-2);

    public List<PreciseSonarData> getCollector() {
        return this.collector;
    }

    public void addData(int measurement) {
        window1 = handleMeasurement(window1, measurement);
        window2 = handleMeasurement(window2, measurement);
        window3 = handleMeasurement(window3, measurement);
    }

    private SonarWindow handleMeasurement(SonarWindow window, int measurement) {
        if (window.getCounter() < 2) {
            window.setMeasurement(measurement);
        } else {
            window.setMeasurement(measurement);
            collector.add(window.toPreciseSonarData());
            window = new SonarWindow(0);
        }
        return window;
    }

}

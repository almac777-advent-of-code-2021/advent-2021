package io.almac.adventofcode.v2021.day1;

import java.util.List;

public class PreciseSonarDataInterpreter {

    private final PreciseSonarDataFactory preciseSonarDataFactory = new PreciseSonarDataFactory();
    private final SimpleSonarInterpreter simpleSonarInterpreter = new SimpleSonarInterpreter();

    public long interpretData(List<Integer> sonarData) {
        sonarData.forEach(preciseSonarDataFactory::addData);
        final var preciseList = preciseSonarDataFactory.getCollector()
                .stream()
                .filter(PreciseSonarData::isRelevant)
                .map(PreciseSonarData::sum)
                .toList();
        return simpleSonarInterpreter.interpretData(preciseList);
    }

}

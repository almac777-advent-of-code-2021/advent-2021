package io.almac.adventofcode.v2021.day1;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class PreciseSonarData {

    int firstMeasurement;
    int secondMeasurement;
    int thirdMeasurement;

    public boolean isRelevant() {
        return firstMeasurement > 0 && secondMeasurement > 0 && thirdMeasurement > 0;
    }

    public int sum() {
        return firstMeasurement + secondMeasurement + thirdMeasurement;
    }

}

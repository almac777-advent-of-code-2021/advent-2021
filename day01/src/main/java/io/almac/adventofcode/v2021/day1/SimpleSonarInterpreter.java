package io.almac.adventofcode.v2021.day1;

import java.util.List;

public class SimpleSonarInterpreter {

    private final SimpleSonarDataFactory sonarDataFactory = new SimpleSonarDataFactory();

    public long interpretData(List<Integer> sonarData) {
        sonarData.forEach(sonarDataFactory::addData);
        return sonarDataFactory.getData()
                .stream()
                .filter(SimpleSonarData::isRelevant)
                .count();
    }

}

package io.almac.adventofcode.v2021.day1;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class SimpleSonarData {

    int previousMeasurement;
    int currentMeasurement;

    boolean isRelevant() {
        return currentMeasurement > previousMeasurement;
    }

}

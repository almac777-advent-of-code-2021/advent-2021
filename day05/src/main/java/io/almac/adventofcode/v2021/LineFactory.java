package io.almac.adventofcode.v2021;

public abstract class LineFactory {

    public Line createLine(ProtoLine protoLine) {
        return createLine(protoLine.getStart(), protoLine.getEnd());
    }

    public abstract Line createLine(MapPoint start, MapPoint end);

    public boolean isButAPoint(MapPoint start, MapPoint end) {
        return start.getX() == end.getX() && start.getY() == end.getY();
    }

    public boolean isStraightLine(MapPoint start, MapPoint end) {
        return start.getX() == end.getX() || start.getY() == end.getY();
    }

    public boolean isVerticalLine(MapPoint start, MapPoint end) {
        return start.getX() == end.getX() && start.getY() != end.getY();
    }

    public boolean isHorizontalLine(MapPoint start, MapPoint end) {
        return start.getX() != end.getX() && start.getY() == end.getY();
    }

    public boolean isEndAbove(MapPoint start, MapPoint end) {
        return start.getX() == end.getX() && start.getY() > end.getY();
    }

    public boolean isEndToTheLeft(MapPoint start, MapPoint end) {
        return start.getX() > end.getX() && start.getY() == end.getY();
    }

    public boolean isDiagonalType1(MapPoint start, MapPoint end) {
        return start.getX() < end.getX() && start.getY() < end.getY()
                || isReversedDiagonalType1(start, end);
    }

    public boolean isReversedDiagonalType1(MapPoint start, MapPoint end) {
        return end.getX() < start.getX() && end.getY() < start.getY();
    }

    public boolean isDiagonalType2(MapPoint start, MapPoint end) {
        return start.getX() < end.getX() && start.getY() > end.getY()
                || isReversedDiagonalType2(start, end);
    }

    public boolean isReversedDiagonalType2(MapPoint start, MapPoint end) {
        return end.getX() < start.getX() && end.getY() > start.getY();
    }
}

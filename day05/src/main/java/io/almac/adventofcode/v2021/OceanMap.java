package io.almac.adventofcode.v2021;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
public class OceanMap {

    private Map<MapPoint, Integer> map = new HashMap<>();

    public void plot(Line line) {
        for (var point : line.getPoints()) {
            map.putIfAbsent(point, 0);
            map.computeIfPresent(point, (p, v) -> ++v);
        }
    }

    public Long overlap() {
        return map.values()
                .stream()
                .filter(v -> v > 1)
                .count();
    }

}

package io.almac.adventofcode.v2021;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

public class StraightLineFactory extends LineFactory {

    @Override
    public Line createLine(MapPoint start, MapPoint end) {
        if (!isStraightLine(start, end)) {
            return null;
        }

        final var points = new LinkedList<MapPoint>();

        if (isButAPoint(start, end)) {
            points.add(start);
            return Line.builder()
                    .points(points)
                    .start(start)
                    .end(end)
                    .build();
        }

        points.add(start);
        if (isHorizontalLine(start, end)) {
            points.addAll(buildHorizontalLines(start, end));
        } else if (isVerticalLine(start, end)) {
            points.addAll(buildVerticalLines(start, end));
        } else {
            throw new RuntimeException();
        }
        points.add(end);
        return Line.builder()
                .start(start)
                .end(end)
                .points(points)
                .build();
    }

    private List<MapPoint> buildHorizontalLines(MapPoint start, MapPoint end) {
        final var res = new LinkedList<MapPoint>();
        Function<Integer, Integer> growth = (prev) -> ++prev;
        if (isEndToTheLeft(start, end)) {
            growth = (prev) -> --prev;
        }
        // start one further
        var i = growth.apply(start.getX());
        while (i != end.getX()) {
            res.add(MapPoint.builder()
                    .x(i)
                    .y(start.getY())
                    .build()
            );
            i = growth.apply(i);
        }
        return res;
    }

    private List<MapPoint> buildVerticalLines(MapPoint start, MapPoint end) {
        final var res = new LinkedList<MapPoint>();
        Function<Integer, Integer> growth = (prev) -> ++prev;
        if (isEndAbove(start, end)) {
            growth = (prev) -> --prev;
        }
        // start one further
        var i = growth.apply(start.getY());
        while (i != end.getY()) {
            res.add(MapPoint.builder()
                    .x(start.getX())
                    .y(i)
                    .build()
            );
            i = growth.apply(i);
        }
        return res;
    }
}

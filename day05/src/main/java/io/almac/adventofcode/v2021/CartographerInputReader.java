package io.almac.adventofcode.v2021;

import io.almac.adventofcode.v2021.utils.FileInputReader;
import io.almac.adventofcode.v2021.utils.InputReader;

import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

public class CartographerInputReader {

    private final InputReader inputReader = new FileInputReader();
    private final static Pattern pattern = Pattern.compile("(\\d+,\\d+) -> (\\d+,\\d+)");
    private final static int START_MATCH_GROUP = 1;
    private final static int END_MATCH_GROUP = 2;

    public List<ProtoLine> readInput() {
        return Optional.of(inputReader.readLines("day5/input"))
                .orElseThrow()
                .stream()
                .map(this::handleLine)
                .toList();
    }

    public ProtoLine handleLine(String line) {
        final var m = pattern.matcher(line);
        if (! m.find()) {
            throw new RuntimeException("Handle line not successful > " + line);
        }
        return ProtoLine.of(
                handlePoint(m.group(START_MATCH_GROUP)),
                handlePoint(m.group(END_MATCH_GROUP))
        );
    }

    public MapPoint handlePoint(String coordinates) {
        final var xy = coordinates.split(",");
        return MapPoint.builder()
                .x(Integer.parseInt(xy[0]))
                .y(Integer.parseInt(xy[1]))
                .build();
    }

}

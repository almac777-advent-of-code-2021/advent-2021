package io.almac.adventofcode.v2021;

import lombok.Value;

@Value(staticConstructor = "of")
public class ProtoLine {

    MapPoint start;
    MapPoint end;

}

package io.almac.adventofcode.v2021;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

public class AnyLineFactory extends LineFactory {

    private final StraightLineFactory straight = new StraightLineFactory();

    @Override
    public Line createLine(MapPoint start, MapPoint end) {
        if (isHorizontalLine(start, end) || isVerticalLine(start, end)) {
            return straight.createLine(start, end);
        }

        final var points = new LinkedList<MapPoint>();

        if (isButAPoint(start, end)) {
            points.add(start);
            return Line.builder()
                    .points(points)
                    .start(start)
                    .end(end)
                    .build();
        }

        points.add(start);
        if (isDiagonalType1(start, end)) {
            points.addAll(handleDiagonalType1(start, end));
        } else if (isDiagonalType2(start, end)) {
            points.addAll(handleDiagonalType2(start, end));
        } else {
            throw new RuntimeException();
        }
        points.add(end);
        return Line.builder()
                .start(start)
                .end(end)
                .points(points)
                .build();
    }

    private List<MapPoint> handleDiagonalType1(MapPoint start, MapPoint end) {
        final var res = new LinkedList<MapPoint>();
        Function<Integer, Integer> growth = (prev) -> ++prev;
        if (isReversedDiagonalType1(start, end)) {
            growth = (prev) -> --prev;
        }
        buildLines(start, end, res, growth, growth);
        return res;
    }

    private void buildLines(MapPoint start, MapPoint end, LinkedList<MapPoint> res, Function<Integer, Integer> growthX, Function<Integer, Integer> growthY) {
        var i = growthX.apply(start.getX());
        var j = growthY.apply(start.getY());
        while (i != end.getX()) {
            res.add(MapPoint.builder()
                    .x(i)
                    .y(j)
                    .build()
            );
            i = growthX.apply(i);
            j = growthY.apply(j);
        }
    }

    private List<MapPoint> handleDiagonalType2(MapPoint start, MapPoint end) {
        final var res = new LinkedList<MapPoint>();
        Function<Integer, Integer> growthX = (prev) -> ++prev;
        Function<Integer, Integer> growthY = (prev) -> --prev;
        if (isReversedDiagonalType2(start, end)) {
            growthX = (prev) -> --prev;
            growthY = (prev) -> ++prev;
        }
        buildLines(start, end, res, growthX, growthY);
        return res;
    }

}

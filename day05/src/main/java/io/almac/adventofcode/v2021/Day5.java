package io.almac.adventofcode.v2021;

import java.util.Objects;
import java.util.Optional;

public class Day5 {

    public static void main(String[] args) {
        final var oceanMap = new OceanMap();
        final var cartographerInputReader = new CartographerInputReader();
        final var lineFactory = new StraightLineFactory();

        Optional.of(cartographerInputReader)
                .map(CartographerInputReader::readInput)
                .orElseThrow()
                .stream()
                .map(lineFactory::createLine)
                .filter(Objects::nonNull)
                .forEach(oceanMap::plot);

        System.out.println("Answer 1 > " + oceanMap.overlap());

        final var secondOceanMap = new OceanMap();
        final var allLineFactory = new AnyLineFactory();

        Optional.of(cartographerInputReader)
                .map(CartographerInputReader::readInput)
                .orElseThrow()
                .stream()
                .map(allLineFactory::createLine)
                .filter(Objects::nonNull)
                .forEach(secondOceanMap::plot);

        System.out.println("Answer 2 > " + secondOceanMap.overlap());
    }

}

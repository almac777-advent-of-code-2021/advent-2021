package io.almac.adventofcode.v2021;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class StraightLineTest {

    private StraightLineFactory straightLineFactory;

    @BeforeEach
    void setup() {
        straightLineFactory = new StraightLineFactory();
    }

    @Test
    void testLineOrientations() {
        final var start = MapPoint.builder()
                .x(1)
                .y(1)
                .build();
        final var pointDownwards = MapPoint.builder()
                .x(start.getX() + 0)
                .y(start.getY() + 1)
                .build();
        final var pointUpwards = MapPoint.builder()
                .x(start.getX() + 0)
                .y(start.getY() - 1)
                .build();
        final var pointLeft = MapPoint.builder()
                .x(start.getX() - 1)
                .y(start.getY() + 0)
                .build();
        final var pointRight = MapPoint.builder()
                .x(start.getX() + 1)
                .y(start.getY() + 0)
                .build();
        final var diagonalLeftUp = MapPoint.builder()
                .x(start.getX() - 1)
                .y(start.getY() - 1)
                .build();
        final var diagonalRightUp = MapPoint.builder()
                .x(start.getX() + 1)
                .y(start.getY() - 1)
                .build();
        final var diagonalLeftDown = MapPoint.builder()
                .x(start.getX() - 1)
                .y(start.getY() + 1)
                .build();
        final var diagonalRightDown = MapPoint.builder()
                .x(start.getX() + 1)
                .y(start.getY() + 1)
                .build();

        assertThat(straightLineFactory.isStraightLine(start, pointDownwards)).isTrue();
        assertThat(straightLineFactory.isStraightLine(start, pointUpwards)).isTrue();
        assertThat(straightLineFactory.isStraightLine(start, pointRight)).isTrue();
        assertThat(straightLineFactory.isStraightLine(start, pointLeft)).isTrue();

        assertThat(straightLineFactory.isStraightLine(start, diagonalRightUp)).isFalse();
        assertThat(straightLineFactory.isStraightLine(start, diagonalRightDown)).isFalse();
        assertThat(straightLineFactory.isStraightLine(start, diagonalLeftDown)).isFalse();
        assertThat(straightLineFactory.isStraightLine(start, diagonalLeftUp)).isFalse();

        assertThat(straightLineFactory.isButAPoint(start, pointDownwards)).isFalse();
        assertThat(straightLineFactory.isButAPoint(start, start)).isTrue();

        assertThat(straightLineFactory.isVerticalLine(start, pointDownwards)).isTrue();
        assertThat(straightLineFactory.isVerticalLine(start, pointUpwards)).isTrue();
        assertThat(straightLineFactory.isVerticalLine(start, pointRight)).isFalse();
        assertThat(straightLineFactory.isVerticalLine(start, pointLeft)).isFalse();

        assertThat(straightLineFactory.isVerticalLine(start, diagonalRightUp)).isFalse();
        assertThat(straightLineFactory.isVerticalLine(start, diagonalRightDown)).isFalse();
        assertThat(straightLineFactory.isVerticalLine(start, diagonalLeftDown)).isFalse();
        assertThat(straightLineFactory.isVerticalLine(start, diagonalLeftUp)).isFalse();

        assertThat(straightLineFactory.isHorizontalLine(start, pointDownwards)).isFalse();
        assertThat(straightLineFactory.isHorizontalLine(start, pointUpwards)).isFalse();
        assertThat(straightLineFactory.isHorizontalLine(start, pointRight)).isTrue();
        assertThat(straightLineFactory.isHorizontalLine(start, pointLeft)).isTrue();

        assertThat(straightLineFactory.isHorizontalLine(start, diagonalRightUp)).isFalse();
        assertThat(straightLineFactory.isHorizontalLine(start, diagonalRightDown)).isFalse();
        assertThat(straightLineFactory.isHorizontalLine(start, diagonalLeftDown)).isFalse();
        assertThat(straightLineFactory.isHorizontalLine(start, diagonalLeftUp)).isFalse();

        assertThat(straightLineFactory.isEndAbove(start, pointDownwards)).isFalse();
        assertThat(straightLineFactory.isEndAbove(start, pointUpwards)).isTrue();
        assertThat(straightLineFactory.isEndAbove(start, pointRight)).isFalse();
        assertThat(straightLineFactory.isEndAbove(start, pointLeft)).isFalse();

        assertThat(straightLineFactory.isEndAbove(start, diagonalRightUp)).isFalse();
        assertThat(straightLineFactory.isEndAbove(start, diagonalRightDown)).isFalse();
        assertThat(straightLineFactory.isEndAbove(start, diagonalLeftDown)).isFalse();
        assertThat(straightLineFactory.isEndAbove(start, diagonalLeftUp)).isFalse();

        assertThat(straightLineFactory.isEndToTheLeft(start, pointDownwards)).isFalse();
        assertThat(straightLineFactory.isEndToTheLeft(start, pointUpwards)).isFalse();
        assertThat(straightLineFactory.isEndToTheLeft(start, pointRight)).isFalse();
        assertThat(straightLineFactory.isEndToTheLeft(start, pointLeft)).isTrue();

        assertThat(straightLineFactory.isEndToTheLeft(start, diagonalRightUp)).isFalse();
        assertThat(straightLineFactory.isEndToTheLeft(start, diagonalRightDown)).isFalse();
        assertThat(straightLineFactory.isEndToTheLeft(start, diagonalLeftDown)).isFalse();
        assertThat(straightLineFactory.isEndToTheLeft(start, diagonalLeftUp)).isFalse();

        final var res = straightLineFactory.createLine(start, pointRight);
        assertThat(res.getStart()).isEqualTo(start);
        assertThat(res.getEnd()).isEqualTo(pointRight);
        assertThat(res.getPoints())
                .isNotNull()
                .isNotEmpty()
                .hasSize(2)
                .containsExactlyElementsOf(
                        List.of(start, pointRight)
                );

        // ---

        final var anyLineFactory = new AnyLineFactory();

        assertThat(anyLineFactory.isDiagonalType1(start, pointDownwards)).isFalse();
        assertThat(anyLineFactory.isDiagonalType1(start, pointUpwards)).isFalse();
        assertThat(anyLineFactory.isDiagonalType1(start, pointRight)).isFalse();
        assertThat(anyLineFactory.isDiagonalType1(start, pointLeft)).isFalse();

        assertThat(anyLineFactory.isDiagonalType1(start, diagonalRightUp)).isFalse();
        assertThat(anyLineFactory.isDiagonalType1(start, diagonalRightDown)).isTrue();
        assertThat(anyLineFactory.isDiagonalType1(start, diagonalLeftDown)).isFalse();
        assertThat(anyLineFactory.isDiagonalType1(start, diagonalLeftUp)).isTrue();


        assertThat(anyLineFactory.isDiagonalType2(start, pointDownwards)).isFalse();
        assertThat(anyLineFactory.isDiagonalType2(start, pointUpwards)).isFalse();
        assertThat(anyLineFactory.isDiagonalType2(start, pointRight)).isFalse();
        assertThat(anyLineFactory.isDiagonalType2(start, pointLeft)).isFalse();

        assertThat(anyLineFactory.isDiagonalType2(start, diagonalRightUp)).isTrue();
        assertThat(anyLineFactory.isDiagonalType2(start, diagonalRightDown)).isFalse();
        assertThat(anyLineFactory.isDiagonalType2(start, diagonalLeftDown)).isTrue();
        assertThat(anyLineFactory.isDiagonalType2(start, diagonalLeftUp)).isFalse();
    }
}

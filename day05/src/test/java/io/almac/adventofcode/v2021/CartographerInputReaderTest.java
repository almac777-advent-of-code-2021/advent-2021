package io.almac.adventofcode.v2021;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class CartographerInputReaderTest {

    private CartographerInputReader cartographerInputReader;

    @BeforeEach
    void seutp() {
        cartographerInputReader = new CartographerInputReader();
    }

    @Test
    void handleLine() {
        final var line = "7,0 -> 7,4";
        assertThat(cartographerInputReader.handleLine(line))
                .isNotNull()
                .isEqualTo(
                        ProtoLine.of(
                                MapPoint.builder()
                                        .x(7)
                                        .y(0)
                                        .build(),
                                MapPoint.builder()
                                        .x(7)
                                        .y(4)
                                        .build()
                        )
                );
    }

    @Test
    void testReadInput() {
        assertThat(cartographerInputReader.readInput())
                .isNotNull()
                .isNotEmpty()
                .hasSize(10)
                .containsExactlyElementsOf(
                        List.of(
                                ProtoLine.of(
                                        MapPoint.builder()
                                                .x(0)
                                                .y(9)
                                                .build(),
                                        MapPoint.builder()
                                                .x(5)
                                                .y(9)
                                                .build()
                                ),
                                ProtoLine.of(
                                        MapPoint.builder()
                                                .x(8)
                                                .y(0)
                                                .build(),
                                        MapPoint.builder()
                                                .x(0)
                                                .y(8)
                                                .build()
                                ),
                                ProtoLine.of(
                                        MapPoint.builder()
                                                .x(9)
                                                .y(4)
                                                .build(),
                                        MapPoint.builder()
                                                .x(3)
                                                .y(4)
                                                .build()
                                ),
                                ProtoLine.of(
                                        MapPoint.builder()
                                                .x(2)
                                                .y(2)
                                                .build(),
                                        MapPoint.builder()
                                                .x(2)
                                                .y(1)
                                                .build()
                                ),
                                ProtoLine.of(
                                        MapPoint.builder()
                                                .x(7)
                                                .y(0)
                                                .build(),
                                        MapPoint.builder()
                                                .x(7)
                                                .y(4)
                                                .build()
                                ),
                                ProtoLine.of(
                                        MapPoint.builder()
                                                .x(6)
                                                .y(4)
                                                .build(),
                                        MapPoint.builder()
                                                .x(2)
                                                .y(0)
                                                .build()
                                ),
                                ProtoLine.of(
                                        MapPoint.builder()
                                                .x(0)
                                                .y(9)
                                                .build(),
                                        MapPoint.builder()
                                                .x(2)
                                                .y(9)
                                                .build()
                                ),
                                ProtoLine.of(
                                        MapPoint.builder()
                                                .x(3)
                                                .y(4)
                                                .build(),
                                        MapPoint.builder()
                                                .x(1)
                                                .y(4)
                                                .build()
                                ),
                                ProtoLine.of(
                                        MapPoint.builder()
                                                .x(0)
                                                .y(0)
                                                .build(),
                                        MapPoint.builder()
                                                .x(8)
                                                .y(8)
                                                .build()
                                ),
                                ProtoLine.of(
                                        MapPoint.builder()
                                                .x(5)
                                                .y(5)
                                                .build(),
                                        MapPoint.builder()
                                                .x(8)
                                                .y(2)
                                                .build()
                                )
                        )
                );
    }

}
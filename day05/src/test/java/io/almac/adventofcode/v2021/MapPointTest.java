package io.almac.adventofcode.v2021;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MapPointTest {

    @Test
    void testEquals() {
        final var x = MapPoint.builder()
                .x(1)
                .y(1)
                .build();

        final var y = MapPoint.builder()
                .x(1)
                .y(1)
                .build();

        assertThat(x.equals(y)).isTrue();
    }

    @Test
    void addingToHashMap() {
        final var x = MapPoint.builder()
                .x(1)
                .y(1)
                .build();

        final var y = MapPoint.builder()
                .x(1)
                .y(1)
                .build();

        final var oceanMap = new OceanMap();
        final var lineFactory = new StraightLineFactory();
        final var line1 = lineFactory.createLine(x, y);
        final var line2 = lineFactory.createLine(x, y);
        oceanMap.plot(line1);
        oceanMap.plot(line2);

        System.out.println(oceanMap.overlap());
    }
}

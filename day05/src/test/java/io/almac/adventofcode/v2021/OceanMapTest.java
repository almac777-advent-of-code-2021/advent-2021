package io.almac.adventofcode.v2021;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Objects;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

class OceanMapTest {

    private OceanMap oceanMap;

    @BeforeEach
    void setup() {
        oceanMap = new OceanMap();
    }

    @Test
    void testPlot() {
        final var lineFactory = new StraightLineFactory();
        final var line = lineFactory.createLine(
                MapPoint.builder()
                        .x(0)
                        .y(0)
                        .build(),
                MapPoint.builder()
                        .x(0)
                        .y(1)
                        .build()
        );
        oceanMap.plot(line);
        assertThat(oceanMap.getMap().values())
                .isNotEmpty()
                .hasSize(2);
    }

    @Test
    void overlap() {
        final var lineFactory = new StraightLineFactory();
        final var line = lineFactory.createLine(
                MapPoint.builder()
                        .x(0)
                        .y(0)
                        .build(),
                MapPoint.builder()
                        .x(0)
                        .y(1)
                        .build()
        );

        oceanMap.plot(line);
        oceanMap.plot(line);

        assertThat(oceanMap.overlap())
                .isEqualTo(2);
    }

    @Test
    void exampleFromWebsite() {
        final var oceanMap = new OceanMap();
        final var cartographerInputReader = new CartographerInputReader();
        final var lineFactory = new StraightLineFactory();

        Optional.of(cartographerInputReader)
                .map(CartographerInputReader::readInput)
                .orElseThrow()
                .stream()
                .map(lineFactory::createLine)
                .filter(Objects::nonNull)
                .forEach(oceanMap::plot);

        assertThat(oceanMap.overlap()).isEqualTo(5);
    }

    @Test
    void exampleFromWebsiteWithDiagonals() {
        final var oceanMap = new OceanMap();
        final var cartographerInputReader = new CartographerInputReader();
        final var lineFactory = new AnyLineFactory();

        Optional.of(cartographerInputReader)
                .map(CartographerInputReader::readInput)
                .orElseThrow()
                .stream()
                .map(lineFactory::createLine)
                .filter(Objects::nonNull)
                .forEach(oceanMap::plot);

        assertThat(oceanMap.overlap()).isEqualTo(12);
    }
}
import io.almac.adventofcode.v2021.day11.CavernMap;
import io.almac.adventofcode.v2021.day11.CavernMapConverter;
import io.almac.adventofcode.v2021.day11.FlashPredictor;
import io.almac.adventofcode.v2021.utils.FileInputReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CompleteTestCase {

    @Test
    void doSmolTestcase() {
        final var targetTicks = 2;
        final var fileInputReader = new FileInputReader();
        final var lines = fileInputReader.readLinesAsListOfInteger("day11/smol-input");
        final var cavernMap = CavernMapConverter.convert(lines);
        final var flashPredictor = new FlashPredictor();
        final var res = flashPredictor.predictFlashes(cavernMap, targetTicks);
        assertThat(res).isEqualTo(9);
    }

    @Test
    void doNormalTestcase() {
        final var targetTicks = 100;
        final var fileInputReader = new FileInputReader();
        final var lines = fileInputReader.readLinesAsListOfInteger("day11/input");
        final var cavernMap = CavernMapConverter.convert(lines);
        final var flashPredictor = new FlashPredictor();
        final var res = flashPredictor.predictFlashes(cavernMap, targetTicks);
        assertThat(res).isEqualTo(1_656);
    }

    @Test
    void doSynchronizationTest() {
        final var fileInputReader = new FileInputReader();
        final var lines = fileInputReader.readLinesAsListOfInteger("day11/input");
        final var cavernMap = CavernMapConverter.convert(lines);
        final var flashPredictor = new FlashPredictor();
        final var res = flashPredictor.predictSynchronization(cavernMap);
        assertThat(res).isEqualTo(195);
    }

}

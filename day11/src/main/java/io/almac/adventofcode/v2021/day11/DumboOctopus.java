package io.almac.adventofcode.v2021.day11;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Optional;
import java.util.function.Consumer;

@Accessors(fluent = true)
@Builder
@Getter
@Setter
public class DumboOctopus {

    private MapPoint position;

    private DumboOctopus topLeft;
    private DumboOctopus top;
    private DumboOctopus topRight;

    private DumboOctopus left;
    private int center;
    private DumboOctopus right;

    private DumboOctopus bottomLeft;
    private DumboOctopus bottom;
    private DumboOctopus bottomRight;

    int numberOfTimesFlashed = 0;

    public void tick() {
        center++;
        if (center == 10) {
            center++;
            flash();
        }
    }

    public void reset() {
        if (center > 9) {
            center = 0;
            numberOfTimesFlashed++;
        }
    }

    private void flash() {
        Consumer<DumboOctopus> flash = (octopus) -> Optional.ofNullable(octopus).ifPresent(DumboOctopus::tick);
        flash.accept(topLeft);
        flash.accept(top);
        flash.accept(topRight);
        flash.accept(left);
        flash.accept(right);
        flash.accept(bottomLeft);
        flash.accept(bottom);
        flash.accept(bottomRight);
    }
}

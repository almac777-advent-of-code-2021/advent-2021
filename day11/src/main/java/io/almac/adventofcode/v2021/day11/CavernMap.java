package io.almac.adventofcode.v2021.day11;

import lombok.Value;

import java.util.Map;

@Value(staticConstructor = "of")
public class CavernMap {

    Map<MapPoint, DumboOctopus> octopusMap;

}

package io.almac.adventofcode.v2021.day11;

import io.almac.adventofcode.v2021.utils.FileInputReader;

public class Day11 {

    public static void main(String[] args) {
        part1();
        part2();
    }

    private static void part1() {
        final var targetTicks = 100;
        final var fileInputReader = new FileInputReader();
        final var lines = fileInputReader.readLinesAsListOfInteger("day11/input");
        final var cavernMap = CavernMapConverter.convert(lines);
        final var flashPredictor = new FlashPredictor();
        final var numberTimesFlashed = flashPredictor.predictFlashes(cavernMap, targetTicks);
        System.out.println("Answer 1 > " + numberTimesFlashed);
    }

    private static void part2() {
        final var fileInputReader = new FileInputReader();
        final var lines = fileInputReader.readLinesAsListOfInteger("day11/input");
        final var cavernMap = CavernMapConverter.convert(lines);
        final var flashPredictor = new FlashPredictor();
        final var synchronizedAtIteration = flashPredictor.predictSynchronization(cavernMap);
        System.out.println("Answer 2 > " + synchronizedAtIteration);
    }

}

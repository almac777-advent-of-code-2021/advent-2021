package io.almac.adventofcode.v2021.day11;

import java.util.Map;

public class FlashPredictor {

    public int predictFlashes(CavernMap cavernMap, int ticks) {
        Map<MapPoint, DumboOctopus> octopusMap = cavernMap.getOctopusMap();
        for (var i = 0; i < ticks; i++) {
            tick(octopusMap);
        }
        return octopusMap.values()
                .stream()
                .map(DumboOctopus::numberOfTimesFlashed)
                .reduce(0, Integer::sum);
    }

    private void tick(Map<MapPoint, DumboOctopus> octopusMap) {
        for (var p : octopusMap.keySet()) {
            octopusMap.get(p).tick();
        }
        for (var p : octopusMap.keySet()) {
            octopusMap.get(p).reset();
        }
    }

    public int predictSynchronization(CavernMap cavernMap) {
        Map<MapPoint, DumboOctopus> octopusMap = cavernMap.getOctopusMap();
        var areDesynchronized = true;
        var i = 0;
        while (areDesynchronized) {
            tick(octopusMap);
            for (var o : octopusMap.values()) {
                if (o.center() != 0) {
                    areDesynchronized = true;
                    break;
                }
                areDesynchronized = false;
            }
            i++;
        }
        return i;
    }
}

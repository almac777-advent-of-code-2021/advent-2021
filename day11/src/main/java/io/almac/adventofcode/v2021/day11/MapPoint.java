package io.almac.adventofcode.v2021.day11;

import lombok.Value;

@Value(staticConstructor = "of")
public class MapPoint {
    int x;
    int y;
}

package io.almac.adventofcode.v2021.day11;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

public class CavernMapConverter {

    public static CavernMap convert(List<List<Integer>> input) {
        Map<MapPoint, DumboOctopus> map = lowResolutionPass(input);
        highResolutionPass(map);
        return CavernMap.of(map);
    }

    private static void highResolutionPass(Map<MapPoint, DumboOctopus> map) {
        MapPoint max = map.keySet()
                .stream()
                .max((p1, p2) -> p1.getX() > p2.getX() && p1.getY() > p2.getY() ? 1 : p1.equals(p2) ? 0 : -1)
                .orElseThrow();
        Predicate<MapPoint> inBounds = (p) -> p.getX() >= 0 && p.getY() >= 0 && p.getX() <= max.getX() && p.getY() <= max.getY();

        Function<MapPoint, MapPoint> goLeft = (p) -> MapPoint.of(p.getX() - 1, p.getY());
        Function<MapPoint, MapPoint> goRight = (p) -> MapPoint.of(p.getX() + 1, p.getY());
        Function<MapPoint, MapPoint> goTop = (p) -> MapPoint.of(p.getX(), p.getY() - 1);
        Function<MapPoint, MapPoint> goBottom = (p) -> MapPoint.of(p.getX(), p.getY() + 1);

        Function<MapPoint, MapPoint> goTopLeft = (p) -> goTop.apply(goLeft.apply(p));
        Function<MapPoint, MapPoint> goBottomLeft = (p) -> goBottom.apply(goLeft.apply(p));
        Function<MapPoint, MapPoint> goTopRight = (p) -> goTop.apply(goRight.apply(p));
        Function<MapPoint, MapPoint> goBottomRight = (p) -> goBottom.apply(goRight.apply(p));

        BiFunction<MapPoint, Function<MapPoint, MapPoint>, DumboOctopus> goAndRetrieve = (p, f) -> inBounds.test(f.apply(p)) ? map.get(f.apply(p)) : null;

        Function<MapPoint, DumboOctopus> left = (p) -> goAndRetrieve.apply(p, goLeft);
        Function<MapPoint, DumboOctopus> right = (p) -> goAndRetrieve.apply(p, goRight);
        Function<MapPoint, DumboOctopus> top = (p) -> goAndRetrieve.apply(p, goTop);
        Function<MapPoint, DumboOctopus> bottom = (p) -> goAndRetrieve.apply(p, goBottom);

        Function<MapPoint, DumboOctopus> topLeft = (p) -> goAndRetrieve.apply(p, goTopLeft);
        Function<MapPoint, DumboOctopus> topRight = (p) -> goAndRetrieve.apply(p, goTopRight);
        Function<MapPoint, DumboOctopus> bottomLeft = (p) -> goAndRetrieve.apply(p, goBottomLeft);
        Function<MapPoint, DumboOctopus> bottomRight = (p) -> goAndRetrieve.apply(p, goBottomRight);

        map.replaceAll((p, v) -> v
                .left(left.apply(p))
                .right(right.apply(p))
                .bottom(bottom.apply(p))
                .bottomLeft(bottomLeft.apply(p))
                .bottomRight(bottomRight.apply(p))
                .top(top.apply(p))
                .topLeft(topLeft.apply(p))
                .topRight(topRight.apply(p)));
    }

    private static Map<MapPoint, DumboOctopus> lowResolutionPass(List<List<Integer>> input) {
        Map<MapPoint, DumboOctopus> map = new LinkedHashMap<>();

        var x = 0;
        for (var rows : input) {
            var y = 0;
            for (var col : rows) {
                var point = MapPoint.of(x, y);
                var octopus = DumboOctopus.builder()
                        .center(col)
                        .position(point)
                        .build();
                map.put(point, octopus);
                y++;
            }
            x++;
        }

        return map;
    }

}

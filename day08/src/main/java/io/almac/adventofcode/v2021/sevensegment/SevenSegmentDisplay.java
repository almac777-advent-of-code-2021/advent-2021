package io.almac.adventofcode.v2021.sevensegment;

public interface SevenSegmentDisplay {
    String buildOutput(SevenSegmentDigit input);
}

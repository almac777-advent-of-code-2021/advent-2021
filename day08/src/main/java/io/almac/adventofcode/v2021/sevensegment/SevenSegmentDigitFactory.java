package io.almac.adventofcode.v2021.sevensegment;

public class SevenSegmentDigitFactory {
    public SevenSegmentDigit buildSevenSegmentDigit(String input) {
        final var digit = Digits.byRepresentation(input);
        if (digit == null) {
            return null;
        }
        switch (digit) {
            case ZERO -> {
                return SevenSegmentDigit.builder()
                        .isTop(true)
                        .isTopLeft(true)
                        .isTopRight(true)
                        .isMiddle(false)
                        .isBottomLeft(true)
                        .isBottomRight(true)
                        .isBottom(true)
                        .build();
            }
            case ONE -> {
                return SevenSegmentDigit.builder()
                        .isTop(false)
                        .isTopLeft(false)
                        .isTopRight(true)
                        .isMiddle(false)
                        .isBottomLeft(false)
                        .isBottomRight(true)
                        .isBottom(false)
                        .build();
            }
            case TWO -> {
                return SevenSegmentDigit.builder()
                        .isTop(true)
                        .isTopLeft(false)
                        .isTopRight(true)
                        .isMiddle(true)
                        .isBottomLeft(true)
                        .isBottomRight(false)
                        .isBottom(true)
                        .build();
            }
            case THREE -> {
                return SevenSegmentDigit.builder()
                        .isTop(true)
                        .isTopLeft(false)
                        .isTopRight(true)
                        .isMiddle(true)
                        .isBottomLeft(false)
                        .isBottomRight(true)
                        .isBottom(true)
                        .build();
            }
            case FOUR -> {
                return SevenSegmentDigit.builder()
                        .isTop(false)
                        .isTopLeft(true)
                        .isTopRight(true)
                        .isMiddle(true)
                        .isBottomLeft(false)
                        .isBottomRight(true)
                        .isBottom(false)
                        .build();
            }
            case FIVE -> {
                return SevenSegmentDigit.builder()
                        .isTop(true)
                        .isTopLeft(true)
                        .isTopRight(false)
                        .isMiddle(true)
                        .isBottomLeft(false)
                        .isBottomRight(true)
                        .isBottom(true)
                        .build();
            }
            case SIX -> {
                return SevenSegmentDigit.builder()
                        .isTop(true)
                        .isTopLeft(true)
                        .isTopRight(false)
                        .isMiddle(true)
                        .isBottomLeft(true)
                        .isBottomRight(true)
                        .isBottom(true)
                        .build();
            }
            case SEVEN -> {
                return SevenSegmentDigit.builder()
                        .isTop(true)
                        .isTopLeft(false)
                        .isTopRight(true)
                        .isMiddle(false)
                        .isBottomLeft(false)
                        .isBottomRight(true)
                        .isBottom(false)
                        .build();
            }
            case EIGHT -> {
                return SevenSegmentDigit.builder()
                        .isTop(true)
                        .isTopLeft(true)
                        .isTopRight(true)
                        .isMiddle(true)
                        .isBottomLeft(true)
                        .isBottomRight(true)
                        .isBottom(true)
                        .build();
            }
            case NINE -> {
                return SevenSegmentDigit.builder()
                        .isTop(true)
                        .isTopLeft(true)
                        .isTopRight(true)
                        .isMiddle(true)
                        .isBottomLeft(false)
                        .isBottomRight(true)
                        .isBottom(true)
                        .build();
            }
        }
        return null;
    }
}

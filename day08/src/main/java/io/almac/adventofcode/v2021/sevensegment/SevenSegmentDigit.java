package io.almac.adventofcode.v2021.sevensegment;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class SevenSegmentDigit {
    static final char TOP = 'a';
    static final char TOP_LEFT = 'b';
    static final char TOP_RIGHT = 'c';
    static final char MIDDLE = 'd';
    static final char BOTTOM_LEFT = 'e';
    static final char BOTTOM_RIGHT = 'f';
    static final char BOTTOM = 'g';
    static final char NEUTRAL = '.';

    boolean isTop;
    boolean isTopLeft;
    boolean isTopRight;
    boolean isMiddle;
    boolean isBottomLeft;
    boolean isBottomRight;
    boolean isBottom;
}

package io.almac.adventofcode.v2021;

import io.almac.adventofcode.v2021.sevensegment.SevenSegmentDigitFactory;
import io.almac.adventofcode.v2021.sevensegment.SevenSegmentDisplay;
import io.almac.adventofcode.v2021.sevensegment.SevenSegmentPrettyDisplay;
import io.almac.adventofcode.v2021.sevensegment.SevenSegmentStringDisplay;

import java.util.List;

public class Day8 {
    public static void main(String[] args) {
        if (args.length > 0) {
            sevenSegmentDisplayPlayground();
            separator();
        }
        part1();
        part2();
    }

    private static void part2() {
        final var inputReader = new SignalInputReader();
        final var signalWrappers = inputReader.readSignals();

        var sum = 0;
        for (var signalWrapper : signalWrappers) {
            var complexDeduction = new ComplexDeduction();
            complexDeduction.deduce(signalWrapper);
            sum += complexDeduction.answer(signalWrapper);
        }

        System.out.println("Answer 2 > " + sum);
    }

    private static void part1() {
        final var inputReader = new SignalInputReader();
        final var simpleDeduction = new SimpleDeduction();
        inputReader.readSignals()
                .stream()
                .map(SignalWrapper::getOutput)
                .forEach(simpleDeduction::deduce);
        System.out.println("Answer 1 > " + simpleDeduction.amountOfFoundDigits());
    }

    private static void separator() {
        System.out.println("======================");
        System.out.println();
    }

    private static void sevenSegmentDisplayPlayground() {
        final var digitFactory = new SevenSegmentDigitFactory();
        final var zero = digitFactory.buildSevenSegmentDigit("0");
        final var one = digitFactory.buildSevenSegmentDigit("1");
        final var two = digitFactory.buildSevenSegmentDigit("2");
        final var three = digitFactory.buildSevenSegmentDigit("3");
        final var four = digitFactory.buildSevenSegmentDigit("4");
        final var five = digitFactory.buildSevenSegmentDigit("5");
        final var six = digitFactory.buildSevenSegmentDigit("6");
        final var seven = digitFactory.buildSevenSegmentDigit("7");
        final var eight = digitFactory.buildSevenSegmentDigit("8");
        final var nine = digitFactory.buildSevenSegmentDigit("9");

        final var digits = List.of(zero, one, two, three, four, five, six, seven, eight, nine);

        SevenSegmentDisplay sevenSegmentDisplay = new SevenSegmentStringDisplay();
        for (final var digit : digits) {
            System.out.println(sevenSegmentDisplay.buildOutput(digit));
        }

        System.out.println();

        sevenSegmentDisplay = new SevenSegmentPrettyDisplay();
        for (final var digit : digits) {
            System.out.print(sevenSegmentDisplay.buildOutput(digit));
        }
    }
}

package io.almac.adventofcode.v2021.sevensegment;

public class SevenSegmentStringDisplay implements SevenSegmentDisplay {

    @Override
    public String buildOutput(SevenSegmentDigit digit) {
        var res = "";
        res += digit.isTop() ? SevenSegmentDigit.TOP : "";
        res += digit.isTopLeft() ? SevenSegmentDigit.TOP_LEFT : "";
        res += digit.isTopRight() ? SevenSegmentDigit.TOP_RIGHT : "";
        res += digit.isMiddle() ? SevenSegmentDigit.MIDDLE : "";
        res += digit.isBottomLeft() ? SevenSegmentDigit.BOTTOM_LEFT : "";
        res += digit.isBottomRight() ? SevenSegmentDigit.BOTTOM_RIGHT : "";
        res += digit.isBottom() ? SevenSegmentDigit.BOTTOM : "";
        return res;
    }
}

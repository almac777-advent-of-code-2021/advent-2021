package io.almac.adventofcode.v2021.rules;

import io.almac.adventofcode.v2021.SegmentConfiguration;
import io.almac.adventofcode.v2021.SegmentConfigurationHelper;

public class DeduceTopLeftOrMiddle {

    public SegmentConfiguration deduce(SegmentConfiguration one,
                                       SegmentConfiguration four) {
        return SegmentConfigurationHelper.notInCommon(one, four);
    }

}

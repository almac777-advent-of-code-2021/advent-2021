package io.almac.adventofcode.v2021;

import io.almac.adventofcode.v2021.utils.FileInputReader;
import io.almac.adventofcode.v2021.utils.InputReader;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class SignalInputReader {

    private final InputReader inputReader = new FileInputReader();

    public List<SignalWrapper> readSignals(String input) {
        return inputReader.readLines(input)
                .stream()
                .map(this::toSignalWrapper)
                .toList();

    }

    public List<SignalWrapper> readSignals() {
        return this.readSignals("day8/input");

    }

    private SignalWrapper toSignalWrapper(String rawLine) {
        final var tokens = rawLine.split(SignalWrapper.SEPARATOR);
        return SignalWrapper.builder()
                .raw(rawLine)
                .rawInput(tokens[0])
                .rawOutput(tokens[1])
                .input(toSignal(tokens[0]))
                .output(toSignal(tokens[1]))
                .build();
    }

    private Signal toSignal(String input) {
        final var tokens = input.split(" ");
        final var res = Arrays.stream(tokens)
                .filter(Objects::nonNull)
                .filter(s -> !s.isBlank())
                .toList();
        return Signal.builder()
                .signals(res)
                .build();
    }

}

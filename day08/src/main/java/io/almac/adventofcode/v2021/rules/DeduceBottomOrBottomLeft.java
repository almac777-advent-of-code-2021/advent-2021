package io.almac.adventofcode.v2021.rules;

import io.almac.adventofcode.v2021.SegmentConfiguration;
import io.almac.adventofcode.v2021.SegmentConfigurationHelper;
import lombok.Getter;

@Getter
public class DeduceBottomOrBottomLeft {

    public SegmentConfiguration deduce(SegmentConfiguration four,
                       SegmentConfiguration eight,
                       SegmentConfiguration top) {
        var topBottomLeftAndBottom = SegmentConfigurationHelper.notInCommon(four, eight);
        return SegmentConfigurationHelper.notInCommon(topBottomLeftAndBottom, top);
    }

}

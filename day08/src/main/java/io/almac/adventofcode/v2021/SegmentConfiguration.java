package io.almac.adventofcode.v2021;

import lombok.*;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SegmentConfiguration {

    private String signal = "abcedfg";

    public static SegmentConfiguration of(String signal) {
        return SegmentConfiguration.builder()
                .signal(signal)
                .build();
    }

    public void reduceTo(String possibleSignals) {
        StringBuilder sb = new StringBuilder();
        for (var i = 0; i < possibleSignals.length(); i++) {
            var lookup = String.valueOf(possibleSignals.charAt(i));
            if (signal.contains(lookup)) {
                sb.append(lookup);
            }
        }
        signal = sb.toString();
    }

    public void reduceBy(String reduceBy) {
        if (reduceBy == null || reduceBy.isBlank()) {
            return;
        }
        int position = signal.indexOf(reduceBy);
        signal = signal.substring(position == 0 ? reduceBy.length() : 0, position == 0 ? reduceBy.length() + 1 : position)
                .concat(signal.substring(position == 0 ? reduceBy.length() + 1 : position + reduceBy.length()));
    }

    public boolean isFixated() {
        return signal.length() == 1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SegmentConfiguration that = (SegmentConfiguration) o;
        if (signal == null) {
            return that.signal == null;
        }

        boolean areStringsEqual = signal.equals(that.signal);
        if (areStringsEqual) {
            return true;
        }

        var areStillEqual = true;
        for (var i = 0; i < signal.length() && areStillEqual; i++) {
            var character = String.valueOf(signal.charAt(i));
            areStillEqual = that.signal.contains(character);
        }
        return areStillEqual;
    }

    @Override
    public int hashCode() {
        if (signal == null) {
            return 0;
        }
        var sum = 0;
        for (var i = 0; i < signal.length(); i++) {
            sum += signal.charAt(i) * (1000 - signal.charAt(i));
        }
        return sum;
    }

    @Override
    public String toString() {
        return signal;
    }
}

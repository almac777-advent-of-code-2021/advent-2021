package io.almac.adventofcode.v2021;

import io.almac.adventofcode.v2021.sevensegment.Digits;

import java.util.List;
import java.util.Optional;

import static io.almac.adventofcode.v2021.sevensegment.Digits.*;

public class SevenSegmentDecoderByLength {

    public List<Digits> deduceSevenSegmentDigitsForSignal(String signal) {
        switch (signal.length()) {
            case 2 -> {
                return List.of(ONE);
            }
            case 3 -> {
                return List.of(SEVEN);
            }
            case 4 -> {
                return List.of(FOUR);
            }
            case 5 -> {
                return List.of(TWO, THREE, FIVE);
            }
            case 6 -> {
                return List.of(ZERO, SIX, NINE);
            }
            case 7 -> {
                return List.of(EIGHT);
            }
        }
        throw new RuntimeException("Signal ?! -> " + signal);
    }
}

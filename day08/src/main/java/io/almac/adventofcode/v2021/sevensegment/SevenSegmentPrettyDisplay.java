package io.almac.adventofcode.v2021.sevensegment;

public class SevenSegmentPrettyDisplay implements SevenSegmentDisplay {

    @Override
    public String buildOutput(SevenSegmentDigit input) {
        char[][] display = new char[11][6];
        var rowIndex = 0;
        if (input.isTop()) {
            rowIndex = fillHorizontal(SevenSegmentDigit.TOP, display, 0);
        } else {
            rowIndex = fillHorizontal(SevenSegmentDigit.NEUTRAL, display, 0);
        }
        var topVerticalIndex = rowIndex;
        if (input.isTopLeft()) {
            fillVerticalLeft(SevenSegmentDigit.TOP_LEFT, display, topVerticalIndex);
        } else {
            fillVerticalLeft(SevenSegmentDigit.NEUTRAL, display, topVerticalIndex);
        }
        if (input.isTopRight()) {
            rowIndex = fillVerticalRight(SevenSegmentDigit.TOP_RIGHT, display, topVerticalIndex);
        } else {
            rowIndex = fillVerticalRight(SevenSegmentDigit.NEUTRAL, display, topVerticalIndex);
        }
        if (input.isMiddle()) {
            rowIndex = fillHorizontal(SevenSegmentDigit.MIDDLE, display, rowIndex);
        } else {
            rowIndex = fillHorizontal(SevenSegmentDigit.NEUTRAL, display, rowIndex);
        }
        var bottomVerticalIndex = rowIndex;
        if (input.isBottomLeft()) {
            fillVerticalLeft(SevenSegmentDigit.BOTTOM_LEFT, display, bottomVerticalIndex);
        } else {
            fillVerticalLeft(SevenSegmentDigit.NEUTRAL, display, bottomVerticalIndex);
        }
        if (input.isBottomRight()) {
            rowIndex = fillVerticalRight(SevenSegmentDigit.BOTTOM_RIGHT, display, bottomVerticalIndex);
        } else {
            rowIndex = fillVerticalRight(SevenSegmentDigit.NEUTRAL, display, bottomVerticalIndex);
        }
        if (input.isBottom()) {
            fillHorizontal(SevenSegmentDigit.BOTTOM, display, rowIndex);
        } else {
            fillHorizontal(SevenSegmentDigit.NEUTRAL, display, rowIndex);
        }
        return displayNeatly(display);
    }

    private int fillHorizontal(char toDisplay, char[][] display, int rowIndex) {
        display[rowIndex][0] = ' ';
        display[rowIndex][1] = toDisplay;
        display[rowIndex][2] = toDisplay;
        display[rowIndex][3] = toDisplay;
        display[rowIndex][4] = toDisplay;
        display[rowIndex][5] = ' ';
        rowIndex++;
        return rowIndex;
    }

    private void fillVerticalLeft(char toDisplay, char[][] display, int rowIndex) {
        for (var i = 0; i < 4; i++, rowIndex++) {
            display[rowIndex][0] = toDisplay;
            for (var j = 1; j < 5; j++) {
                display[rowIndex][j] = ' ';
            }
        }
    }

    private int fillVerticalRight(char toDisplay, char[][] display, int rowIndex) {
        for (var i = 0; i < 4; i++, rowIndex++) {
            display[rowIndex][5] = toDisplay;
        }
        return rowIndex;
    }

    private String displayNeatly(char[][] display) {
        StringBuilder res = new StringBuilder();
        for (char[] row : display) {
            for (char column : row) {
                res.append(column);
            }
            res.append("\n");
        }
        res.append("\n");
        return res.toString();
    }
}

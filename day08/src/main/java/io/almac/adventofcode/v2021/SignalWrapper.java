package io.almac.adventofcode.v2021;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class SignalWrapper {
    public static final String SEPARATOR = "\\|";

    String raw;
    String rawInput;
    String rawOutput;

    Signal input;
    Signal output;
}

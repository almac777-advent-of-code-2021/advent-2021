package io.almac.adventofcode.v2021;

import io.almac.adventofcode.v2021.rules.*;
import io.almac.adventofcode.v2021.sevensegment.Digits;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static io.almac.adventofcode.v2021.SegmentPosition.*;
import static io.almac.adventofcode.v2021.sevensegment.Digits.*;

public class ComplexDeduction {

    private Map<Digits, Set<SegmentPosition>> digitsSetMap = new HashMap<>();
    private Map<SegmentConfiguration, Digits> resultingMap = new HashMap<>();

    public ComplexDeduction() {
        digitsSetMap.put(ONE, Set.of(TOP_RIGHT, BOTTOM_RIGHT));
        digitsSetMap.put(TWO, Set.of(TOP, TOP_RIGHT, MIDDLE, BOTTOM_LEFT, BOTTOM));
        digitsSetMap.put(THREE, Set.of(TOP, TOP_RIGHT, MIDDLE, BOTTOM_RIGHT, BOTTOM));
        digitsSetMap.put(FOUR, Set.of(TOP_LEFT, TOP_RIGHT, MIDDLE, BOTTOM_RIGHT));
        digitsSetMap.put(FIVE, Set.of(TOP, TOP_LEFT, MIDDLE, BOTTOM_RIGHT, BOTTOM));
        digitsSetMap.put(SIX, Set.of(TOP, TOP_LEFT, MIDDLE, BOTTOM_LEFT, BOTTOM_RIGHT, BOTTOM));
        digitsSetMap.put(SEVEN, Set.of(TOP, TOP_RIGHT, BOTTOM_RIGHT));
        digitsSetMap.put(EIGHT, Set.of(TOP, TOP_LEFT, TOP_RIGHT, MIDDLE, BOTTOM_LEFT, BOTTOM_RIGHT, BOTTOM));
        digitsSetMap.put(NINE, Set.of(TOP, TOP_LEFT, TOP_RIGHT, MIDDLE, BOTTOM_RIGHT, BOTTOM));
        digitsSetMap.put(ZERO, Set.of(TOP, TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT, BOTTOM));
    }

    public void deduce(SignalWrapper signalWrapper) {
        final var deductionByCounting = new SimpleDeduction();
        final Map<SegmentPosition, SegmentConfiguration> positionToConfiguration = new HashMap<>();
        deductionByCounting.deduce(signalWrapper.getInput());
        deductionByCounting.deduce(signalWrapper.getOutput());

        final var one = deductionByCounting.segmentConfigurationFor(ONE).stream().findFirst().orElseThrow();
        final var four = deductionByCounting.segmentConfigurationFor(FOUR).stream().findFirst().orElseThrow();
        final var seven = deductionByCounting.segmentConfigurationFor(SEVEN).stream().findFirst().orElseThrow();
        final var eight = deductionByCounting.segmentConfigurationFor(EIGHT).stream().findFirst().orElseThrow();

        final var zeroSixNine = deductionByCounting.segmentConfigurationFor(ZERO);

        final var top = new DeduceTopPosition().deduceTopPosition(
                one,
                seven
        );
        final var topLeftOrMiddle = new DeduceTopLeftOrMiddle().deduce(
                one,
                four
        );
        final var bottomOrBottomLeft = new DeduceBottomOrBottomLeft().deduce(
                four,
                eight,
                top
        );
        final var topLeftAndMiddle = new DeduceTopLeftAndMiddleFromZeroSixNineAndTopLeftOrMiddle();
        topLeftAndMiddle.deduce(
                zeroSixNine,
                topLeftOrMiddle
        );
        final var bottomLeftAndBottom = new DeduceBottomLeftAndBottomFromZeroSixNineAndBottomLeftOrBottom();
        bottomLeftAndBottom.deduce(
                zeroSixNine,
                bottomOrBottomLeft
        );
        final var topRightAndBottomRight = new DeduceTopRightAndBottomRightFromZeroSixNineAndOne();
        topRightAndBottomRight.deduce(
                zeroSixNine,
                one
        );

        positionToConfiguration.put(TOP, top);
        positionToConfiguration.put(TOP_LEFT, topLeftAndMiddle.getTopLeft());
        positionToConfiguration.put(TOP_RIGHT, topRightAndBottomRight.getTopRight());
        positionToConfiguration.put(MIDDLE, topLeftAndMiddle.getMiddle());
        positionToConfiguration.put(BOTTOM_LEFT, bottomLeftAndBottom.getBottomLeft());
        positionToConfiguration.put(BOTTOM_RIGHT, topRightAndBottomRight.getBottomRight());
        positionToConfiguration.put(BOTTOM, bottomLeftAndBottom.getBottom());

        for (final var digit : Digits.values()) {
            final var sb = new StringBuilder();
            final var segmentPositionsForDigit = digitsSetMap.get(digit);
            for (final var position : segmentPositionsForDigit) {
                final var segmentConfiguration = positionToConfiguration.get(position);
                if (!segmentConfiguration.isFixated()) {
                    throw new RuntimeException("Deduction did not work");
                }
                sb.append(segmentConfiguration);
            }
            resultingMap.put(SegmentConfiguration.of(sb.toString()), digit);
        }
    }
    // egcfdba (EIGHT) vs. gbcfae
    public int answer(SignalWrapper signalWrapper) {
        final var output = signalWrapper.getOutput().getSignals();
        var sum = 0;
        for (int i = 0, multiplicator = 1_000; i < output.size(); i++, multiplicator /= 10) {
            final var codedNumber = SegmentConfiguration.of(output.get(i));
            sum += resultingMap.get(codedNumber).getValue() * multiplicator;
        }
        return sum;
    }
}

package io.almac.adventofcode.v2021.rules;

import io.almac.adventofcode.v2021.SegmentConfiguration;
import io.almac.adventofcode.v2021.SegmentConfigurationHelper;
import lombok.Getter;

import java.util.Set;

@Getter
public class DeduceBottomLeftAndBottomFromZeroSixNineAndBottomLeftOrBottom {

    private SegmentConfiguration bottomLeft;
    private SegmentConfiguration bottom;

    public SegmentConfiguration deduce(Set<SegmentConfiguration> zeroSixNine, SegmentConfiguration bottomLeftOrBottom) {
        for (var e : zeroSixNine) {
            final var res = SegmentConfigurationHelper.common(e, bottomLeftOrBottom);
            if (res.isFixated()) {
                bottomLeft = SegmentConfigurationHelper.notInCommon(res, bottomLeftOrBottom);
                bottom = res;
                return bottom;
            }
        }
        return null;
    }

}

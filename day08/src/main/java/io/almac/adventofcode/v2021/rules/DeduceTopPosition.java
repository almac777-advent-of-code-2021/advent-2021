package io.almac.adventofcode.v2021.rules;

import io.almac.adventofcode.v2021.SegmentConfiguration;
import io.almac.adventofcode.v2021.SegmentConfigurationHelper;

public class DeduceTopPosition {

    public SegmentConfiguration deduceTopPosition(SegmentConfiguration one, SegmentConfiguration seven) {
        return SegmentConfigurationHelper.notInCommon(one, seven);
    }

}

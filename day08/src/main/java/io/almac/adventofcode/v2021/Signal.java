package io.almac.adventofcode.v2021;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class Signal {

    List<String> signals;
}

package io.almac.adventofcode.v2021.rules;

import io.almac.adventofcode.v2021.SegmentConfiguration;
import io.almac.adventofcode.v2021.SegmentConfigurationHelper;
import lombok.Getter;

import java.util.Set;

@Getter
public class DeduceTopRightAndBottomRightFromZeroSixNineAndOne {

    private SegmentConfiguration topRight;
    private SegmentConfiguration bottomRight;

    public SegmentConfiguration deduce(Set<SegmentConfiguration> zeroSixNine, SegmentConfiguration one) {
        for (var e : zeroSixNine) {
            final var res = SegmentConfigurationHelper.common(e, one);
            if (res.isFixated()) {
                topRight = SegmentConfigurationHelper.notInCommon(res, one);
                bottomRight = res;
                return bottomRight;
            }
        }
        return null;
    }

}

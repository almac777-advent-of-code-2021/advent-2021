package io.almac.adventofcode.v2021.rules;

import io.almac.adventofcode.v2021.SegmentConfiguration;
import io.almac.adventofcode.v2021.SegmentConfigurationHelper;
import lombok.Getter;

import java.util.Set;

@Getter
public class DeduceTopLeftAndMiddleFromZeroSixNineAndTopLeftOrMiddle {

    private SegmentConfiguration middle;
    private SegmentConfiguration topLeft;

    public SegmentConfiguration deduce(Set<SegmentConfiguration> zeroSixNine, SegmentConfiguration topLeftOrMiddle) {
        for (var e : zeroSixNine) {
            final var res = SegmentConfigurationHelper.common(e, topLeftOrMiddle);
            if (res.isFixated()) {
                middle = SegmentConfigurationHelper.notInCommon(res, topLeftOrMiddle);
                topLeft = res;
                return topLeft;
            }
        }
        return null;
    }

}

package io.almac.adventofcode.v2021.sevensegment;

import lombok.Getter;

@Getter
public enum Digits {
    ZERO("0", 0),
    ONE("1", 1),
    TWO("2", 2),
    THREE("3", 3),
    FOUR("4", 4),
    FIVE("5", 5),
    SIX("6", 6),
    SEVEN("7", 7),
    EIGHT("8", 8),
    NINE("9", 9);

    private String representation;
    private Integer value;

    Digits(String representation, Integer value) {
        this.value = value;
        this.representation = representation;
    }

    public static Digits byRepresentation(String input) {
        if (input == null || input.isBlank()) {
            return null;
        }
        switch (input) {
            case "0" -> {
                return ZERO;
            }
            case "1" -> {
                return ONE;
            }
            case "2" -> {
                return TWO;
            }
            case "3" -> {
                return THREE;
            }
            case "4" -> {
                return FOUR;
            }
            case "5" -> {
                return FIVE;
            }
            case "6" -> {
                return SIX;
            }
            case "7" -> {
                return SEVEN;
            }
            case "8" -> {
                return EIGHT;
            }
            case "9" -> {
                return NINE;
            }
        }

        return null;
    }
}

package io.almac.adventofcode.v2021;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.function.Predicate;

public class SegmentConfigurationHelper {

    public static Set<SegmentConfiguration> common(Set<SegmentConfiguration> x, Set<SegmentConfiguration> y) {
        final Set<SegmentConfiguration> res = new LinkedHashSet<SegmentConfiguration>();
        for (var xElement : x) {
            for (var yElement : y) {
                res.add(common(xElement, yElement));
            }
        }
        return res;
    }

    public static Set<SegmentConfiguration> notInCommon(Set<SegmentConfiguration> x, Set<SegmentConfiguration> y) {
        final Set<SegmentConfiguration> res = new LinkedHashSet<SegmentConfiguration>();
        for (var xElement : x) {
            for (var yElement : y) {
                res.add(notInCommon(xElement, yElement));
            }
        }
        return res;
    }

    public static SegmentConfiguration common(SegmentConfiguration x, SegmentConfiguration y) {
        var sb = new StringBuilder();
        commonHelperFunction(x, sb, s -> y.getSignal().contains(s));
        return SegmentConfiguration.builder()
                .signal(sb.toString())
                .build();
    }

    public static SegmentConfiguration notInCommon(SegmentConfiguration x, SegmentConfiguration y) {
        var sb = new StringBuilder();
        commonHelperFunction(x, sb, s -> ! y.getSignal().contains(s));
        commonHelperFunction(y, sb, s -> ! x.getSignal().contains(s));
        return SegmentConfiguration.builder()
                .signal(sb.toString())
                .build();
    }

    private static void commonHelperFunction(SegmentConfiguration segmentConfig, StringBuilder sb, Predicate<String> againstOtherSegment) {
        for (var i = 0; i < segmentConfig.getSignal().length(); i++) {
            var tmp = String.valueOf(segmentConfig.getSignal().charAt(i));
            if (againstOtherSegment.test(tmp)) {
                if (sb.indexOf(tmp) < 0) {
                    sb.append(tmp);
                }
            }
        }
    }
}

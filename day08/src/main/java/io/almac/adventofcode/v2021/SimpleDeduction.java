package io.almac.adventofcode.v2021;

import io.almac.adventofcode.v2021.sevensegment.Digits;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Set;

public class SimpleDeduction {

    private final HashMap<Digits, Integer> counter = new HashMap<>();
    private final HashMap<Digits, Set<SegmentConfiguration>> symbols = new HashMap<>();

    public void deduce(Signal signal) {
        final var decoder = new SevenSegmentDecoderByLength();
        for (final var toDeduce : signal.getSignals()) {
            final var deducedDigit = decoder.deduceSevenSegmentDigitsForSignal(toDeduce);
            deducedDigit.forEach((foundDigit) -> {
                symbols.putIfAbsent(foundDigit, new LinkedHashSet<>());
                symbols.computeIfPresent(foundDigit, (k, v) -> {
                    v.add(SegmentConfiguration.of(toDeduce));
                    return v;
                });
            });
            deducedDigit.forEach((foundDigit) -> {
                counter.putIfAbsent(foundDigit, 0);
                counter.computeIfPresent(foundDigit, (k, v) -> ++v);
            });
        }
    }

    public Set<SegmentConfiguration> segmentConfigurationFor(Digits digits) {
        return symbols.get(digits);
    }

    public int amountOfFoundDigits() {
        return counter.get(Digits.ONE) +
               counter.get(Digits.FOUR) +
               counter.get(Digits.SEVEN) +
               counter.get(Digits.EIGHT);
    }
}

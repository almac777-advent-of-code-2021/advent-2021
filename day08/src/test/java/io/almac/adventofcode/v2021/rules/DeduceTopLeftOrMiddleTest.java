package io.almac.adventofcode.v2021.rules;

import io.almac.adventofcode.v2021.SegmentConfiguration;
import io.almac.adventofcode.v2021.SegmentConfigurationHelper;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class DeduceTopLeftOrMiddleTest {

    @Test
    void deduce() {
        final var one = SegmentConfiguration.of("cf");
        final var four = SegmentConfiguration.of("bcdf");
        final var topLeftOrMiddle = SegmentConfiguration.of("bd");
        var actual = new DeduceTopLeftOrMiddle().deduce(one, four);
        assertThat(actual).isEqualTo(topLeftOrMiddle);
    }
}
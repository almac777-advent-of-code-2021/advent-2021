package io.almac.adventofcode.v2021.rules;

import io.almac.adventofcode.v2021.SegmentConfiguration;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class DeduceBottomLeftAndBottomFromZeroSixNineAndBottomLeftOrBottomTest {

    @Test
    void deduce() {
        final var sixNineZero = Set.of(
                SegmentConfiguration.of("abdefg"),
                SegmentConfiguration.of("abcdfg"),
                SegmentConfiguration.of("abcefg")
        );
        final var bottomLeftOrBottom = SegmentConfiguration.of("eg");
        final var resultBottomLeft = SegmentConfiguration.of("e");
        final var resultBottom = SegmentConfiguration.of("g");

        final var deduction = new DeduceBottomLeftAndBottomFromZeroSixNineAndBottomLeftOrBottom();
        deduction.deduce(sixNineZero, bottomLeftOrBottom);

        assertThat(deduction.getBottomLeft()).isEqualTo(resultBottomLeft);
        assertThat(deduction.getBottom()).isEqualTo(resultBottom);
    }
}
package io.almac.adventofcode.v2021;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ComplexDeductionTest {

    private SignalInputReader inputReader;
    private ComplexDeduction complexDeduction;

    @BeforeEach
    void setUp() {
        inputReader = new SignalInputReader();
        complexDeduction = new ComplexDeduction();
    }

    @Test
    void deduceSimple() {
        inputReader = new SignalInputReader();
        var wrappers = inputReader.readSignals("day8/input-part-2");
        var wrapper = wrappers.get(0);
        complexDeduction.deduce(wrapper);
        assertThat(complexDeduction.answer(wrapper)).isEqualTo(5353);
    }

    @Test
    void deduceSampleRow1() {
        inputReader = new SignalInputReader();
        var wrappers = inputReader.readSignals("day8/input");
        var wrapper = wrappers.get(0);
        complexDeduction.deduce(wrapper);
        assertThat(complexDeduction.answer(wrapper)).isEqualTo(8394);
    }

    @Test
    void deduceSampleRow2() {
        inputReader = new SignalInputReader();
        var wrappers = inputReader.readSignals("day8/input");
        var wrapper = wrappers.get(1);
        complexDeduction.deduce(wrapper);
        assertThat(complexDeduction.answer(wrapper)).isEqualTo(9781);
    }

    @Test
    void deduceSampleRow3() {
        inputReader = new SignalInputReader();
        var wrappers = inputReader.readSignals("day8/input");
        var wrapper = wrappers.get(2);
        complexDeduction.deduce(wrapper);
        assertThat(complexDeduction.answer(wrapper)).isEqualTo(1197);
    }

    @Test
    void deduceSampleRow4() {
        inputReader = new SignalInputReader();
        var wrappers = inputReader.readSignals("day8/input");
        var wrapper = wrappers.get(3);
        complexDeduction.deduce(wrapper);
        assertThat(complexDeduction.answer(wrapper)).isEqualTo(9361);
    }

    @Test
    void deduceSampleRow5() {
        inputReader = new SignalInputReader();
        var wrappers = inputReader.readSignals("day8/input");
        var wrapper = wrappers.get(4);
        complexDeduction.deduce(wrapper);
        assertThat(complexDeduction.answer(wrapper)).isEqualTo(4873);
    }

    @Test
    void deduceSampleRow6() {
        inputReader = new SignalInputReader();
        var wrappers = inputReader.readSignals("day8/input");
        var wrapper = wrappers.get(5);
        complexDeduction.deduce(wrapper);
        assertThat(complexDeduction.answer(wrapper)).isEqualTo(8418);
    }

    @Test
    void deduceSampleRow7() {
        inputReader = new SignalInputReader();
        var wrappers = inputReader.readSignals("day8/input");
        var wrapper = wrappers.get(6);
        complexDeduction.deduce(wrapper);
        assertThat(complexDeduction.answer(wrapper)).isEqualTo(4548);
    }

    @Test
    void deduceSampleRow8() {
        inputReader = new SignalInputReader();
        var wrappers = inputReader.readSignals("day8/input");
        var wrapper = wrappers.get(7);
        complexDeduction.deduce(wrapper);
        assertThat(complexDeduction.answer(wrapper)).isEqualTo(1625);
    }

    @Test
    void deduceSampleRow9() {
        inputReader = new SignalInputReader();
        var wrappers = inputReader.readSignals("day8/input");
        var wrapper = wrappers.get(8);
        complexDeduction.deduce(wrapper);
        assertThat(complexDeduction.answer(wrapper)).isEqualTo(8717);
    }

    @Test
    void deduceSampleRow10() {
        inputReader = new SignalInputReader();
        var wrappers = inputReader.readSignals("day8/input");
        var wrapper = wrappers.get(9);
        complexDeduction.deduce(wrapper);
        assertThat(complexDeduction.answer(wrapper)).isEqualTo(4315);
    }

    @Test
    void deduceSampleComplete() {
        inputReader = new SignalInputReader();
        var wrappers = inputReader.readSignals("day8/input");
        var sum = 0;
        for (final var wrapper : wrappers) {
            complexDeduction = new ComplexDeduction();
            complexDeduction.deduce(wrapper);
            sum += complexDeduction.answer(wrapper);
        }
        assertThat(sum).isEqualTo(61_229);
    }
}
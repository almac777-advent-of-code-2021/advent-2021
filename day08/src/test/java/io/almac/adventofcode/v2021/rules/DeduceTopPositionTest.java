package io.almac.adventofcode.v2021.rules;

import io.almac.adventofcode.v2021.SegmentConfiguration;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class DeduceTopPositionTest {

    @Test
    void deduceTopPosition() {
        final var seven = SegmentConfiguration.of("abc");
        final var one = SegmentConfiguration.of("bc");
        final var result = SegmentConfiguration.of("a");

        SegmentConfiguration actual = new DeduceTopPosition().deduceTopPosition(seven, one);
        assertThat(actual).isEqualTo(result);
    }
}
package io.almac.adventofcode.v2021.rules;

import io.almac.adventofcode.v2021.SegmentConfiguration;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class DeduceTopRightAndBottomRightFromZeroSixNineAndOneTest {

    @Test
    void deduce() {
        final var sixNineZero = Set.of(
                SegmentConfiguration.of("abdefg"),
                SegmentConfiguration.of("abcdfg"),
                SegmentConfiguration.of("abcefg")
        );
        final var one = SegmentConfiguration.of("cf");
        final var resultTopRight = SegmentConfiguration.of("c");
        final var resultBottomRight = SegmentConfiguration.of("f");

        final var deduction = new DeduceTopRightAndBottomRightFromZeroSixNineAndOne();
        deduction.deduce(sixNineZero, one);

        assertThat(deduction.getTopRight()).isEqualTo(resultTopRight);
        assertThat(deduction.getBottomRight()).isEqualTo(resultBottomRight);
    }
}
package io.almac.adventofcode.v2021;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SegmentConfigurationHelperTest {

    @Test
    void common_xLongerThanY() {
        final var x = SegmentConfiguration.builder()
                .signal("abc")
                .build();
        final var y = SegmentConfiguration.builder()
                .signal("bc")
                .build();
        assertThat(SegmentConfigurationHelper.common(x, y))
                .hasFieldOrPropertyWithValue("signal", "bc");
    }

    @Test
    void common_yLongerThanX() {
        final var x = SegmentConfiguration.builder()
                .signal("bc")
                .build();
        final var y = SegmentConfiguration.builder()
                .signal("abc")
                .build();
        assertThat(SegmentConfigurationHelper.common(x, y))
                .hasFieldOrPropertyWithValue("signal", "bc");
    }

    @Test
    void notInCommon_xLongerThanY() {
        final var x = SegmentConfiguration.builder()
                .signal("abc")
                .build();
        final var y = SegmentConfiguration.builder()
                .signal("bc")
                .build();
        assertThat(SegmentConfigurationHelper.notInCommon(x, y))
                .hasFieldOrPropertyWithValue("signal", "a");
    }

    @Test
    void notInCommon_yLongerThanX() {
        final var x = SegmentConfiguration.builder()
                .signal("bc")
                .build();
        final var y = SegmentConfiguration.builder()
                .signal("abc")
                .build();
        assertThat(SegmentConfigurationHelper.notInCommon(x, y))
                .hasFieldOrPropertyWithValue("signal", "a");
    }
}
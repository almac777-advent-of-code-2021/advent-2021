package io.almac.adventofcode.v2021;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

class SegmentConfigurationTest {

    @Test
    void testEquals() {
        SegmentConfiguration x = SegmentConfiguration.builder()
                .signal("abc")
                .build();

        SegmentConfiguration y = SegmentConfiguration.builder()
                .signal("cba")
                .build();

        assertThat(x.equals(y)).isTrue();

        SegmentConfiguration a = SegmentConfiguration.builder()
                .signal("xyz")
                .build();

        SegmentConfiguration b = SegmentConfiguration.builder()
                .signal("xya")
                .build();

        assertThat(a.equals(b)).isFalse();
    }

    @Test
    void testHashCode() {
        final var eight = SegmentConfiguration.of("egcfdba");
        final var nine = SegmentConfiguration.of("gbcfae");
        assertThat(eight).isNotEqualTo(nine);
        assertThat(eight.hashCode()).isNotEqualTo(nine.hashCode());
    }
}
package io.almac.adventofcode.v2021.rules;

import io.almac.adventofcode.v2021.SegmentConfiguration;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class DeduceBottomOrBottomLeftTest {
    @Test
    void deduce() {
        final var top = SegmentConfiguration.of("a");
        final var eight = SegmentConfiguration.of("abcdefg");
        final var four = SegmentConfiguration.of("bcdf");
        final var expectedBottomOrBottomLeft = SegmentConfiguration.of("eg");

        var deduction = new DeduceBottomOrBottomLeft();
        var actual = deduction.deduce(four, eight, top);
        assertThat(actual).isEqualTo(expectedBottomOrBottomLeft);
    }
}
package io.almac.adventofcode.v2021.rules;

import io.almac.adventofcode.v2021.SegmentConfiguration;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class DeduceTopLeftAndMiddleFromZeroSixNineAndTopLeftOrMiddleTest {

    @Test
    void deduce() {
        final var sixNineZero = Set.of(
                SegmentConfiguration.of("abdefg"),
                SegmentConfiguration.of("abcdfg"),
                SegmentConfiguration.of("abcefg")
        );
        final var topLeftOrMiddle = SegmentConfiguration.of("bd");
        final var resultTopLeft = SegmentConfiguration.of("b");
        final var resultMiddle = SegmentConfiguration.of("d");

        final var deduction = new DeduceTopLeftAndMiddleFromZeroSixNineAndTopLeftOrMiddle();
        final var actual = deduction.deduce(sixNineZero, topLeftOrMiddle);
        assertThat(actual)
                .isNotNull()
                .isEqualTo(resultTopLeft);
        assertThat(actual).isEqualTo(deduction.getTopLeft());
        assertThat(resultMiddle).isEqualTo(deduction.getMiddle());
    }
}
package io.almac.adventofcode.v2021;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class SimpleDeductionTest {

    @Test
    void deduce() {
        final var inputReader = new SignalInputReader();
        final var simpleDeduction = new SimpleDeduction();
        inputReader.readSignals()
                .stream()
                .map(SignalWrapper::getOutput)
                .forEach(simpleDeduction::deduce);
        assertThat(simpleDeduction.amountOfFoundDigits()).isEqualTo(26);
    }
}
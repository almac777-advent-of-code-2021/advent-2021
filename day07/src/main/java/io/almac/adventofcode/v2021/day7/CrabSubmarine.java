package io.almac.adventofcode.v2021.day7;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CrabSubmarine {

    private int x;
    private int y;

    public int estimatedCostToMoveToPositionX(int x) {
        return Math.abs(this.x - x);
    }

    public int correctCostToMoveToPositionX(int x) {
        return sumFrom1ToN(estimatedCostToMoveToPositionX(x));
    }

    private int sumFrom1ToN(int n) {
        return n * (n + 1) / 2;
    }

}

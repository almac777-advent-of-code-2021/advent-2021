package io.almac.adventofcode.v2021.day7;

import io.almac.adventofcode.v2021.utils.FileInputReader;

public class Day7 {

    public static void main(String[] args) {
        final var crabSimulator = new CrabSubmarineSimulator();
        final var inputReader = new FileInputReader();
        final var input = inputReader.readLinesAsInteger("day7/input");
        final var optimumCost = crabSimulator.optimizeCosts(input, CrabSubmarine::estimatedCostToMoveToPositionX);
        System.out.println("Answer 1 > " + optimumCost);
        final var optimumCostWithCorrectCostFunction = crabSimulator.optimizeCosts(input, CrabSubmarine::correctCostToMoveToPositionX);
        System.out.println("Answer 2 > " + optimumCostWithCorrectCostFunction);
    }

}

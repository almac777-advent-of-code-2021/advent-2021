package io.almac.adventofcode.v2021.day7;

import java.util.List;
import java.util.function.BiFunction;

public class CrabSubmarineSimulator {

    private List<CrabSubmarine> crabSubmarines;

    public int optimizeCosts(List<Integer> inputPositions, BiFunction<CrabSubmarine, Integer, Integer> calculateCost) {
        crabSubmarines = inputPositions.stream()
                .map(i -> CrabSubmarine.builder().x(i).build())
                .toList();
        var max = crabSubmarines.stream()
                .map(CrabSubmarine::getX)
                .max(Integer::compareTo);

        if (max.isEmpty()) {
            return -1;
        }

        var optimumCost = Integer.MAX_VALUE;
        var optimumPosition = 0;
        for (var i = 0; i < max.get(); i++) {
            var currentOptimumCost = 0;
            for (var sub : crabSubmarines) {
                currentOptimumCost += calculateCost.apply(sub, i);
            }
            if (currentOptimumCost < optimumCost) {
                optimumCost = currentOptimumCost;
                optimumPosition = i;
            }
        }
        System.out.println("Cost: " + optimumCost);
        System.out.println("Position: " + optimumPosition);
        return optimumCost;
    }

}

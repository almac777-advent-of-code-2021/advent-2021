package io.almac.adventofcode.v2021.day7;

import io.almac.adventofcode.v2021.utils.FileInputReader;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CrabSubmarineSimulatorTest {

    @Test
    void optimizeCosts() {
        final var crabSimulator = new CrabSubmarineSimulator();
        final var inputReader = new FileInputReader();
        final var input = inputReader.readLinesAsInteger("day7/input");
        final var optimumCost = crabSimulator.optimizeCosts(input, CrabSubmarine::estimatedCostToMoveToPositionX);
        assertThat(optimumCost).isEqualTo(37);
    }

    @Test
    void optimizeCostWithCorrectFunction() {
        final var crabSimulator = new CrabSubmarineSimulator();
        final var inputReader = new FileInputReader();
        final var input = inputReader.readLinesAsInteger("day7/input");
        final var optimumCost = crabSimulator.optimizeCosts(input, CrabSubmarine::correctCostToMoveToPositionX);
        assertThat(optimumCost).isEqualTo(168);
    }
}
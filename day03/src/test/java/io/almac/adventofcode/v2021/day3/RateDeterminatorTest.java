package io.almac.adventofcode.v2021.day3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class RateDeterminatorTest {

    private RateDeterminator rateDeterminator;

    @BeforeEach
    void setup() {
        rateDeterminator = new RateDeterminator();
    }

    @Test
    void determinateGamma() {
        DataColumn dataColumn = DataColumn.builder()
                .columnIndex(0)
                .column(List.of("1", "0", "1", "0", "1"))
                .build();
        final var res = rateDeterminator.determinateRate(dataColumn);
        assertThat(res)
                .isNotNull()
                .hasFieldOrPropertyWithValue("rate", RateType.GAMMA)
                .hasFieldOrPropertyWithValue("valueGamma", 1)
                .hasFieldOrPropertyWithValue("valueEpsilon", 0);
    }

    @Test
    void determinateEpsilon() {
        DataColumn dataColumn = DataColumn.builder()
                .columnIndex(0)
                .column(List.of("1", "0", "1", "0", "0"))
                .build();
        final var res = rateDeterminator.determinateRate(dataColumn);
        assertThat(res)
                .isNotNull()
                .hasFieldOrPropertyWithValue("rate", RateType.EPSILON)
                .hasFieldOrPropertyWithValue("valueGamma", 0)
                .hasFieldOrPropertyWithValue("valueEpsilon", 1);
    }
}
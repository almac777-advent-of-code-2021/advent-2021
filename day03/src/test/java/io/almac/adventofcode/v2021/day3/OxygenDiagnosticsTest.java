package io.almac.adventofcode.v2021.day3;

import io.almac.adventofcode.v2021.utils.FileInputReader;
import io.almac.adventofcode.v2021.utils.InputReader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class OxygenDiagnosticsTest {

    private InputReader inputReader;
    private OxygenDiagnostics oxygenDiagnostics;

    @BeforeEach
    void setup() {
        inputReader = new FileInputReader();
        oxygenDiagnostics = new OxygenDiagnostics();
    }

    @Test
    void runOxygenDiagnostics() {
        final var data = inputReader.readLines("day3/test-input");
        final var res = oxygenDiagnostics.runOxygenDiagnostics(data);
        assertThat(res)
                .isNotNull()
                .isNotBlank()
                .isEqualTo("10111");
    }

    @Test
    void runFirstIterationOfDiagnostics() {
        final var data = inputReader.readLines("day3/test-input");
        final var res = oxygenDiagnostics.filterData(data, 0);
        assertThat(res)
                .isNotNull()
                .isNotEmpty()
                .containsExactlyElementsOf(
                        List.of(
                                "11110",
                                "10110",
                                "10111",
                                "10101",
                                "11100",
                                "10000",
                                "11001"
                        )
                );
    }

    @Test
    void runSecondIterationOfDiagnostics() {
        final var firstIterationData = List.of(
                "11110",
                "10110",
                "10111",
                "10101",
                "11100",
                "10000",
                "11001"
        );
        final var res = oxygenDiagnostics.filterData(firstIterationData, 1);
        assertThat(res)
                .isNotNull()
                .isNotEmpty()
                .containsExactlyElementsOf(
                        List.of(
                                "10110",
                                "10111",
                                "10101",
                                "10000"
                        )
                );
    }
}
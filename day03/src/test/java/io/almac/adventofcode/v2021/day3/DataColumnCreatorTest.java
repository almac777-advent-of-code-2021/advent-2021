package io.almac.adventofcode.v2021.day3;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class DataColumnCreatorTest {

    private DataColumnCreator dataColumnCreator;

    @BeforeEach
    void setup() {
        dataColumnCreator = new DataColumnCreator();
    }

    @Test
    void createBits() {
        final var input = List.of(
                "101",
                "001",
                "011"
        );

        final var result = dataColumnCreator.createBits(input);

        assertThat(result)
                .isNotNull()
                .isNotEmpty()
                .hasSize(3)
                .containsExactlyElementsOf(
                        List.of(
                                DataColumn.builder()
                                        .columnIndex(0)
                                        .column(List.of("1", "0", "0"))
                                        .build(),
                                DataColumn.builder()
                                        .columnIndex(1)
                                        .column(List.of("0", "0", "1"))
                                        .build(),
                                DataColumn.builder()
                                        .columnIndex(2)
                                        .column(List.of("1", "1", "1"))
                                        .build()
                        )
                );
    }
}
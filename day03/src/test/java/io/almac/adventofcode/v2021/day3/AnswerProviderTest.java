package io.almac.adventofcode.v2021.day3;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class AnswerProviderTest {

    @Test
    void getSum() {
        List<Rate> input = List.of(
                Rate.builder()
                        .rate(RateType.GAMMA)
                        .valueGamma(1)
                        .valueEpsilon(0)
                        .build(),
                Rate.builder()
                        .rate(RateType.EPSILON)
                        .valueGamma(0)
                        .valueEpsilon(1)
                        .build(),
                Rate.builder()
                        .rate(RateType.GAMMA)
                        .valueGamma(1)
                        .valueEpsilon(0)
                        .build()
        );

        final var gammaAnswer = AnswerProvider.determineGamma(input);
        final var epsilonAnswer = AnswerProvider.determineEpsilon(input);

        assertThat(gammaAnswer).isEqualTo(5);
        assertThat(epsilonAnswer).isEqualTo(2);
    }
}
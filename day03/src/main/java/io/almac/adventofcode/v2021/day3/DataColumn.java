package io.almac.adventofcode.v2021.day3;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class DataColumn {

    int columnIndex;
    List<String> column;

}

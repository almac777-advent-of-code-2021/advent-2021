package io.almac.adventofcode.v2021.day3;

import java.util.LinkedList;
import java.util.List;

public class DataColumnCreator {

    public List<DataColumn> createBits(List<String> data) {
        final var res = new LinkedList<DataColumn>();
        final var firstElement = data.get(0);
        for (var i = 0; i < firstElement.length(); i++) {
            var column = new LinkedList<String>();
            for (var e : data) {
                column.add(Character.toString(e.charAt(i)));
            }
            res.add(DataColumn.builder()
                    .columnIndex(i)
                    .column(column)
                    .build()
            );
        }
        return res;
    }

}

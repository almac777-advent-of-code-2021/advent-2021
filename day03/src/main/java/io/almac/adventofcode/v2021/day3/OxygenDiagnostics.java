package io.almac.adventofcode.v2021.day3;

import java.util.ArrayList;
import java.util.List;

public class OxygenDiagnostics {

    private final DataColumnCreator dataColumnCreator = new DataColumnCreator();
    private final RateDeterminator rateDeterminator = new RateDeterminator();

    public String runOxygenDiagnostics(List<String> data) {
        final var in = data.get(0);
        List<String> oxygen = new ArrayList<>(data);
        for (var i = 0; i < in.length(); i++) {
            oxygen = filterData(oxygen, i);
            if (oxygen.size() == 1) {
                break;
            }
        }
        return oxygen.get(0);
    }

    public List<String> filterData(List<String> data, int column) {
        final var dataCols = dataColumnCreator.createBits(data);
        final var rate = rateDeterminator.determinateRate(dataCols.get(column));
        final var filterFor = rate.getRate() == RateType.GAMMA || rate.getRate() == RateType.UNDECIDED ? "1" : "0";
        return data.stream()
                .filter(s -> Character.toString(s.charAt(column)).equals(filterFor))
                .toList();
    }

}

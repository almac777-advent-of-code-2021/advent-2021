package io.almac.adventofcode.v2021.day3;

import java.util.ArrayList;
import java.util.List;

public class CO2Diagnostics {

    private final DataColumnCreator dataColumnCreator = new DataColumnCreator();
    private final RateDeterminator rateDeterminator = new RateDeterminator();

    public String runCo2Diagnostics(List<String> data) {
        final var in = data.get(0);
        List<String> co2 = new ArrayList<>(data);
        for (var i = 0; i < in.length(); i++) {
            co2 = filterData(co2, i);
            if (co2.size() == 1) {
                break;
            }
        }
        return co2.get(0);
    }

    public List<String> filterData(List<String> data, int column) {
        final var dataCols = dataColumnCreator.createBits(data);
        final var rate = rateDeterminator.determinateRate(dataCols.get(column));
        final var filterFor = rate.getRate() == RateType.GAMMA || rate.getRate() == RateType.UNDECIDED ? "0" : "1";
        return data.stream()
                .filter(s -> Character.toString(s.charAt(column)).equals(filterFor))
                .toList();
    }

}

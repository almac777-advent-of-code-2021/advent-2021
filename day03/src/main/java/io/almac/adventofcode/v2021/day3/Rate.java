package io.almac.adventofcode.v2021.day3;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Rate {

    RateType rate;
    int columnIndex;
    int valueGamma;
    int valueEpsilon;

}

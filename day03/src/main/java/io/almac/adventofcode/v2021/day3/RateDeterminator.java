package io.almac.adventofcode.v2021.day3;

public class RateDeterminator {

    public Rate determinateRate(DataColumn column) {
        final var ones = column.getColumn().stream().filter(s -> s.equals("1")).count();
        final var zeroes = column.getColumn().stream().filter(s -> s.equals("0")).count();
        RateType rateType;
        if (ones > zeroes) {
            rateType = RateType.GAMMA;
        } else if (ones < zeroes) {
            rateType = RateType.EPSILON;
        } else {
            rateType = RateType.UNDECIDED;
        }
        return Rate.builder()
                .columnIndex(column.getColumnIndex())
                .rate(rateType)
                .valueGamma(rateType == RateType.GAMMA ? 1 : 0)
                .valueEpsilon(rateType == RateType.GAMMA ? 0 : 1)
                .build();
    }

}

package io.almac.adventofcode.v2021.day3;

import io.almac.adventofcode.v2021.utils.FileInputReader;

public class Day3 {

    public static void main(String[] args) {
        final var inputReader = new FileInputReader();
        final var data = inputReader.readLines("day3/input");
        final var product = AnswerProvider.provideAnswer(data);
        final var subsystemProduct = AnswerProvider.provideAnswerForSubsystems(data);
        System.out.println("> Answer Part 1: " + product);
        System.out.println("> Answer Part 2: " + subsystemProduct);
    }

}

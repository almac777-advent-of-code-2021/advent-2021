package io.almac.adventofcode.v2021.day3;

import java.util.List;

public class AnswerProvider {

    public static int provideAnswer(List<String> data) {
        final var nthDataCreator = new DataColumnCreator();
        final var rateDeterminator = new RateDeterminator();
        final var rates = nthDataCreator.createBits(data)
                .stream()
                .map(rateDeterminator::determinateRate)
                .toList();
        final var gamma = determineGamma(rates);
        final var epsilon = determineEpsilon(rates);
        System.out.println("Sum Gamma: " + gamma);
        System.out.println("Sum Epsilon: " + epsilon);
        return gamma * epsilon;
    }

    public static int provideAnswerForSubsystems(List<String> data) {
        return determineCO2(data) * determineOxygen(data);
    }

    public static int determineGamma(List<Rate> rates) {
        final var gamma = rates.stream().reduce("", (str, rate) -> str += rate.getValueGamma(), String::join);
        return Integer.valueOf(gamma, 2);
    }

    public static int determineEpsilon(List<Rate> rates) {
        final var epsilon = rates.stream().reduce("", (str, rate) -> str += rate.getValueEpsilon(), String::join);
        return Integer.valueOf(epsilon, 2);
    }

    public static int determineCO2(List<String> data) {
        final var co2 = (new CO2Diagnostics().runCo2Diagnostics(data));
        final var decimalResult = Integer.valueOf(co2, 2);
        System.out.println("CO2: " + decimalResult);
        return decimalResult;
    }

    public static int determineOxygen(List<String> data) {
        final var oxygen = (new OxygenDiagnostics().runOxygenDiagnostics(data));
        final var decimalResult = Integer.valueOf(oxygen, 2);
        System.out.println("Oxygen: " + decimalResult);
        return decimalResult;
    }

}

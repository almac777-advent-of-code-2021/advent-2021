package io.almac.adventofcode.v2021.utils;

import java.util.List;

public interface InputReader {

    List<String> readLines(String cooridnates);

    List<Integer> readLinesAsInteger(String coordinates);

}

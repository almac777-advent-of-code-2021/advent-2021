package io.almac.adventofcode.v2021.utils;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class FileInputReader implements InputReader {

    @Override
    public List<String> readLines(String coordinates) {
        if (coordinates == null || coordinates.isBlank()) {
            return List.of();
        }

        try (InputStream in = getClass().getClassLoader().getResourceAsStream(coordinates)) {
            if (in != null) {
                return IOUtils.readLines(in, StandardCharsets.UTF_8);
            }
        } catch (IOException ioException) {
            throw new RuntimeException(ioException);
        }
        return List.of();
    }

    @Override
    public List<Integer> readLinesAsInteger(String coordinates) {
        return this.readLines(coordinates)
                .stream()
                .filter(s -> s != null && ! s.isBlank())
                .map(Integer::parseInt)
                .collect(Collectors.toList());
    }

    public List<List<Integer>> readLinesAsListOfInteger(String coordinates) {
        List<List<Integer>> res = new LinkedList<>();
        final var lines = readLines(coordinates);
        for (final var line : lines) {
            List<Integer> row = new LinkedList<>();
            for (var i = 0; i < line.length(); i++) {
                row.add(Integer.valueOf(String.valueOf(line.charAt(i))));
            }
            res.add(row);
        }
        return res;
    }
}

package io.almac.adventofcode.v2021.utils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class FileInputReaderTest {

    private InputReader fileInputReader;

    @BeforeEach
    void setup() {
        fileInputReader = new FileInputReader();
    }

    @Test
    void testInitiation() {
        assertThat(fileInputReader).isNotNull();
    }

    @Test
    void whenInputReaderIsProvidedEmptyNullOrNonExistentFilenames_itShouldReturnEmpty() {
        assertThat(fileInputReader.readLines(""))
                .isNotNull()
                .isEmpty();

        assertThat(fileInputReader.readLines(null))
                .isNotNull()
                .isEmpty();

        assertThat(fileInputReader.readLines("non-existent"))
                .isNotNull()
                .isEmpty();
    }

    @Test
    void whenInputReaderIsCalledWithFileContainingThreeLines_itShouldReturnThreeLines() {
        assertThat(fileInputReader.readLines("day2/test-input"))
                .isNotNull()
                .isNotEmpty()
                .hasSize(3)
                .containsExactlyElementsOf(
                        List.of(
                                "up 1",
                                "down 1",
                                "forward 1"
                        )
                );
    }

    @Test
    void whenInputReaderIsCalledWithFileContainingIntegers_itShouldReturnIntegers() {
        assertThat(fileInputReader.readLinesAsInteger("day1/test-input"))
                .isNotNull()
                .isNotEmpty()
                .hasSize(5)
                .containsExactlyElementsOf(
                        List.of(
                                1,
                                1337,
                                69,
                                420,
                                9999
                        )
                );
    }

}
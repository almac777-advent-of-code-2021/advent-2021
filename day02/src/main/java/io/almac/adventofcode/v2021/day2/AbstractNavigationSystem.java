package io.almac.adventofcode.v2021.day2;

import lombok.Getter;

@Getter
public abstract class AbstractNavigationSystem {

    protected int x = 0;
    protected int y = 0;

    public abstract void applyCommand(SubmarineCommand command);
}

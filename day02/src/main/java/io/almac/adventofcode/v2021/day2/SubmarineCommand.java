package io.almac.adventofcode.v2021.day2;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class SubmarineCommand {

    Movement movement;
    int value;

}

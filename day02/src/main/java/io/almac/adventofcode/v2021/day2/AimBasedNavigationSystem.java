package io.almac.adventofcode.v2021.day2;

import lombok.Getter;

@Getter
public class AimBasedNavigationSystem extends AbstractNavigationSystem {

    private int aim = 0;

    public void applyCommand(SubmarineCommand command) {
        switch (command.getMovement()) {
            case BACKWARD -> {
                x -= command.getValue();
            }
            case FORWARD -> {
                x += command.getValue();
                y += command.getValue() * aim;
            }
            case DOWN -> {
                aim += command.getValue();
            }
            case UP -> {
                aim -= command.getValue();
            }
        }
    }
}

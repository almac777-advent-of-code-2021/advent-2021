package io.almac.adventofcode.v2021.day2;

import io.almac.adventofcode.v2021.day2.Movement;
import io.almac.adventofcode.v2021.day2.SubmarineCommand;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StringToSubmarineCommandConverter {

    private static final int COMMAND_INDEX = 0;
    private static final int VALUE_INDEX = 1;
    private static final int EXPECTED_AMOUNT_OF_TOKENS = 2;
    private static List<String> COMMAND_WHITELIST = Arrays.stream(Movement.values())
            .map(Enum::toString)
            .map(String::toLowerCase)
            .toList();


    public SubmarineCommand toSubmarineCommand(String input) {
        if (input == null || input.isBlank()) {
            return null;
        }

        final var tokens = input.split(" ");

        if (tokens.length != EXPECTED_AMOUNT_OF_TOKENS) {
            System.out.println("invalid input: " + input);
            return null;
        }

        final var command = tokens[COMMAND_INDEX];
        final var value = tokens[VALUE_INDEX];

        if (!COMMAND_WHITELIST.contains(command)) {
            System.out.println("invalid input: " + input);
            return null;
        }

        return SubmarineCommand.builder()
                .movement(Movement.valueOf(command.toUpperCase()))
                .value(Integer.parseInt(value))
                .build();
    }

}

package io.almac.adventofcode.v2021.day2;

import lombok.Getter;

@Getter
public enum Movement {

    BACKWARD("backward"),
    FORWARD("forward"),
    DOWN("down"),
    UP("up");

    private final String command;

    Movement(String command) {
        this.command = command;
    }

}

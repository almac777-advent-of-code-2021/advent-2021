package io.almac.adventofcode.v2021.day2;

import io.almac.adventofcode.v2021.utils.FileInputReader;

public class Day2 {

    public static void main(String[] args) {
        final var inputReader = new FileInputReader();
        final var converter = new StringToSubmarineCommandConverter();
        final var navigationSystem = new NavigationSystem();
        final var aimBasedNavigationSystem = new AimBasedNavigationSystem();

        inputReader.readLines("day2/input")
                .stream()
                .map(converter::toSubmarineCommand)
                .forEach(navigationSystem::applyCommand);

        inputReader.readLines("day2/input")
                .stream()
                .map(converter::toSubmarineCommand)
                .forEach(aimBasedNavigationSystem::applyCommand);

        System.out.println("part 1 -> " + AnswerProvider.answer(navigationSystem));
        System.out.println("part 2 -> " + AnswerProvider.answer(aimBasedNavigationSystem));
    }
}

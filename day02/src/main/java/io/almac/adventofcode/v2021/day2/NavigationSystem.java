package io.almac.adventofcode.v2021.day2;

import lombok.Getter;

public class NavigationSystem extends AbstractNavigationSystem {

    public void applyCommand(SubmarineCommand command) {
        switch (command.getMovement()) {
            case BACKWARD -> {
                x -= command.getValue();
            }
            case FORWARD -> {
                x += command.getValue();
            }
            case DOWN -> {
                y += command.getValue();
            }
            case UP -> {
                y -= command.getValue();
                if (y < 0) {
                    y = 0;
                }
            }
        }
    }

}

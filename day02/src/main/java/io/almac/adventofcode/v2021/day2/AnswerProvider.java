package io.almac.adventofcode.v2021.day2;

public class AnswerProvider {

    public static int answer(AbstractNavigationSystem navigationSystem) {
        return navigationSystem.getX() * navigationSystem.getY();
    }

}

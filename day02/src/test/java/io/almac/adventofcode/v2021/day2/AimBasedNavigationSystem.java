package io.almac.adventofcode.v2021.day2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class AimBasedNavigationSystem {

    private AimBasedNavigationSystem aimBasedNavigationSystem;

    @BeforeEach
    void setup() {
        aimBasedNavigationSystem = new AimBasedNavigationSystem();
    }

    @Test
    void testInit() {
        assertThat(aimBasedNavigationSystem).isNotNull();
    }

    @Test
    void applyCommand() {
    }
}
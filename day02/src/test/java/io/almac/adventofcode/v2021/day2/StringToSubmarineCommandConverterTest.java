package io.almac.adventofcode.v2021.day2;

import io.almac.adventofcode.v2021.day2.Movement;
import io.almac.adventofcode.v2021.day2.StringToSubmarineCommandConverter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class StringToSubmarineCommandConverterTest {

    private StringToSubmarineCommandConverter converter;

    @BeforeEach
    void setup() {
        converter = new StringToSubmarineCommandConverter();
    }

    @Test
    void testSetup() {
        assertThat(converter).isNotNull();
    }

    @Test
    void toSubmarineCommand() {
        final var subCommand1 = converter.toSubmarineCommand("up 1");
        final var subCommand2 = converter.toSubmarineCommand("down 2");
        final var subCommand3 = converter.toSubmarineCommand("forward 3");
        final var subCommand4 = converter.toSubmarineCommand("invalid 123");
        final var subCommand5 = converter.toSubmarineCommand("up 1 down 2");
        final var subCommand6 = converter.toSubmarineCommand("yyyy");
        final var subCommand7 = converter.toSubmarineCommand(null);
        final var subCommand8 = converter.toSubmarineCommand("");
        final var subCommand9 = converter.toSubmarineCommand("1 1");

        assertThat(subCommand1)
                .isNotNull()
                .hasFieldOrPropertyWithValue("movement", Movement.UP)
                .hasFieldOrPropertyWithValue("value", 1);

        assertThat(subCommand2)
                .isNotNull()
                .hasFieldOrPropertyWithValue("movement", Movement.DOWN)
                .hasFieldOrPropertyWithValue("value", 2);

        assertThat(subCommand3)
                .isNotNull()
                .hasFieldOrPropertyWithValue("movement", Movement.FORWARD)
                .hasFieldOrPropertyWithValue("value", 3);

        assertThat(subCommand4).isNull();
        assertThat(subCommand5).isNull();
        assertThat(subCommand6).isNull();
        assertThat(subCommand7).isNull();
        assertThat(subCommand8).isNull();
        assertThat(subCommand9).isNull();
    }
}
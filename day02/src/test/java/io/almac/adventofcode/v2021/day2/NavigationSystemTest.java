package io.almac.adventofcode.v2021.day2;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class NavigationSystemTest {

    private NavigationSystem navigationSystem;

    @BeforeEach
    void setup() {
        navigationSystem = new NavigationSystem();
    }

    @Test
    void testInit() {
        assertThat(navigationSystem).isNotNull();
    }

    @Test
    void applyCommand() {
        navigationSystem.applyCommand(SubmarineCommand.builder()
                    .movement(Movement.FORWARD)
                    .value(1)
                .build());

        assertThat(navigationSystem)
                .hasFieldOrPropertyWithValue("x", 1)
                .hasFieldOrPropertyWithValue("y", 0);

        navigationSystem.applyCommand(SubmarineCommand.builder()
                .movement(Movement.UP)
                .value(1)
                .build());

        assertThat(navigationSystem)
                .hasFieldOrPropertyWithValue("x", 1)
                .hasFieldOrPropertyWithValue("y", 0);

        navigationSystem.applyCommand(SubmarineCommand.builder()
                .movement(Movement.DOWN)
                .value(1)
                .build());

        assertThat(navigationSystem)
                .hasFieldOrPropertyWithValue("x", 1)
                .hasFieldOrPropertyWithValue("y", 1);
    }
}
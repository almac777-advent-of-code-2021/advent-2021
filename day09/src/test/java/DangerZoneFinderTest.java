import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class DangerZoneFinderTest {

    private NavigableUnderwaterMap navigableUnderwaterMap;
    private DangerZoneFinder dangerZoneFinder;
    private List<MapPoint> dangerPoints;

    @BeforeEach
    void setUp() {
        final var underwaterRawDataReader = new UnderwaterRawDataReader();
        final var underwaterMap = underwaterRawDataReader.readData();
        final var converter = new UnderwaterMapToNavigableMapConverter();
        dangerPoints = underwaterMap.dangerPoints();
        navigableUnderwaterMap = converter.convert(underwaterMap);
        dangerZoneFinder = new DangerZoneFinder();
    }

    @Test
    void findDangerZoneTopLeft() {
        final var dangerZone = dangerZoneFinder.identifyDangerZone(MapPoint.of(0, 0), navigableUnderwaterMap);
        assertThat(dangerZone.getBasin())
                .isNotNull()
                .isNotEmpty()
                .hasSize(3);
    }

    @Test
    void findDangerZoneTopRight() {
        final var dangerZone = dangerZoneFinder.identifyDangerZone(MapPoint.of(9, 0), navigableUnderwaterMap);
        assertThat(dangerZone.getBasin())
                .isNotNull()
                .isNotEmpty()
                .hasSize(9);
    }

    @Test
    void findDangerZoneInTheMiddle() {
        final var dangerZone = dangerZoneFinder.identifyDangerZone(MapPoint.of(2, 2), navigableUnderwaterMap);
        assertThat(dangerZone.getBasin())
                .isNotNull()
                .isNotEmpty()
                .hasSize(14);
    }

    @Test
    void findDangerZoneAtTheBottom() {
        final var dangerZone = dangerZoneFinder.identifyDangerZone(MapPoint.of(6, 4), navigableUnderwaterMap);
        assertThat(dangerZone.getBasin())
                .isNotNull()
                .isNotEmpty()
                .hasSize(9);
    }


    @Test
    void findAllDangerPoints() {
        final var dangerZones = dangerZoneFinder.findDangerZones(dangerPoints, navigableUnderwaterMap);
        final var zonesListedBySize = dangerZones.stream()
                .map(DangerZone::getBasin)
                .map(List::size)
                .sorted((x, y) -> Integer.compare(x, y) * -1)
                .toList();
        final var res = zonesListedBySize.get(0) * zonesListedBySize.get(1) * zonesListedBySize.get(2);
        assertThat(res)
                .isEqualTo(1_134);
    }

}
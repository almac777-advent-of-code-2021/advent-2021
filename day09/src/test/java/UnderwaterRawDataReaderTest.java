import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class UnderwaterRawDataReaderTest {

    @Test
    void readData() {
        final var underwaterRawDataReader = new UnderwaterRawDataReader();
        final var res = underwaterRawDataReader.readData();
        assertThat(res.calculateDangerPoints())
                .isEqualTo(15);
    }
}
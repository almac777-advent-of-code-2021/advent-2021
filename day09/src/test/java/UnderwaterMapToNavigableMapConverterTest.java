import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class UnderwaterMapToNavigableMapConverterTest {

    @Test
    void convert() {
        final var underwaterRawDataReader = new UnderwaterRawDataReader();
        final var underwaterMap = underwaterRawDataReader.readData();
        final var converter = new UnderwaterMapToNavigableMapConverter();
        final var navigableUnderwaterMap = converter.convert(underwaterMap);

        assertThat(navigableUnderwaterMap.getNavigableMapZoneMap().keySet().size())
                 .isEqualTo(underwaterMap.getNavigation().keySet().size());

        final var dangerPoints = underwaterMap.dangerPoints();

        dangerPoints.forEach(dangerPoint -> assertThat(navigableUnderwaterMap.getNavigableMapZoneMap().get(dangerPoint)).isNotNull());
    }

    @Test
    void convertKeySet() {
        final var underwaterRawDataReader = new UnderwaterRawDataReader();
        final var underwaterMap = underwaterRawDataReader.readData();
        final var converter = new UnderwaterMapToNavigableMapConverter();

        final var maxMapPoint = converter.findMaximumXYCoordinates(underwaterMap.getNavigation().keySet());

        assertThat(maxMapPoint)
                .isEqualTo(MapPoint.of(9, 4));
    }
}
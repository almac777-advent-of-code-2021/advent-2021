import lombok.Value;

import java.util.List;
import java.util.Map;

@Value(staticConstructor = "of")
public class UnderwaterMap {

    Map<MapPoint, MapZone> navigation;

    public int calculateDangerPoints() {
        return navigation.values()
                .stream()
                .filter(MapZone::checkIsDanger)
                .map(mp -> mp.getCenter() + 1)
                .reduce(0, Integer::sum);
    }

    public List<MapPoint> dangerPoints() {
        return navigation.entrySet()
                .stream()
                .filter((e) -> e.getValue().checkIsDanger())
                .map(Map.Entry::getKey)
                .toList();
    }

}

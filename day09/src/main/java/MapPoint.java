import lombok.Value;

@Value(staticConstructor = "of")
public class MapPoint {
    int x;
    int y;
}

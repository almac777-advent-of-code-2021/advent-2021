import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Builder
@Accessors(fluent = true)
@Getter
@Setter
public class NavigableMapZone {

    private MapZone center;
    private NavigableMapZone above;
    private NavigableMapZone below;
    private NavigableMapZone left;
    private NavigableMapZone right;

}

import java.util.LinkedList;
import java.util.List;
import java.util.function.BiFunction;

public class MapPointConverter {

    private static final int NEUTRAL_ELEMENT = 9;

    public List<MapZone> toMapPoint(List<Integer> previous, List<Integer> current, List<Integer> next, int y) {
        BiFunction<List<Integer>, Integer, Integer> currentCenter = List::get;
        BiFunction<List<Integer>, Integer, Integer> currentLeft = (list, i) -> i != 0 ? list.get(i - 1) : NEUTRAL_ELEMENT;
        BiFunction<List<Integer>, Integer, Integer> currentRight = (list, i) -> i + 1 < list.size() ? list.get(i + 1) : NEUTRAL_ELEMENT;

        BiFunction<List<Integer>, Integer, Integer> aboveOrBelowCenter = (aboveOrBelowList, i) -> aboveOrBelowList != null ? aboveOrBelowList.get(i) : NEUTRAL_ELEMENT;
        BiFunction<List<Integer>, Integer, Integer> aboveOrBelowLeft = (aboveOrBelowList, i) -> aboveOrBelowList != null ? currentLeft.apply(aboveOrBelowList, i) : NEUTRAL_ELEMENT;
        BiFunction<List<Integer>, Integer, Integer> aboveOrBelowRight = (aboveOrBelowList, i) -> aboveOrBelowList != null ? currentRight.apply(aboveOrBelowList, i) : NEUTRAL_ELEMENT;

        final List<MapZone> mapZones = new LinkedList<>();
        for (var i = 0; i < current.size(); i++) {
            int center = currentCenter.apply(current, i);
            int left = currentLeft.apply(current, i);
            int right = currentRight.apply(current, i);

            int previousCenter = aboveOrBelowCenter.apply(previous, i);
            int previousLeft = aboveOrBelowLeft.apply(previous, i);
            int previousRight = aboveOrBelowRight.apply(previous, i);

            int nextCenter = aboveOrBelowCenter.apply(next, i);
            int nextLeft = aboveOrBelowLeft.apply(next, i);
            int nextRight = aboveOrBelowRight.apply(next, i);

            mapZones.add(MapZone.builder()
                    .above(previousCenter)
                    .below(nextCenter)
                    .center(center)
                    .left(left)
                    .right(right)
                    .diagonalDownLeft(nextLeft)
                    .diagonalDownRight(nextRight)
                    .diagonalUpLeft(previousLeft)
                    .diagonalUpRight(previousRight)
                    .coordinates(MapPoint.of(i, y))
                    .build());
        }
        return mapZones;
    }

}

import lombok.Getter;
import lombok.Value;

import java.util.Map;

@Getter
@Value(staticConstructor = "of")
public class NavigableUnderwaterMap {
    Map<MapPoint, NavigableMapZone> navigableMapZoneMap;
}

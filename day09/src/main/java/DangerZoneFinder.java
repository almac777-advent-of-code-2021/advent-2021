import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class DangerZoneFinder {

    public List<DangerZone> findDangerZones(List<MapPoint> dangerZones, NavigableUnderwaterMap underwaterMap) {
        List<DangerZone> res = new ArrayList<>();
        for (var dangerZone : dangerZones) {
            final var identifiedDangerZone = identifyDangerZone(dangerZone, underwaterMap);
            res.add(identifiedDangerZone);
        }
        return res;
    }

    public DangerZone identifyDangerZone(MapPoint dangerZone, NavigableUnderwaterMap underwaterMap) {
        final var start = underwaterMap.getNavigableMapZoneMap().get(dangerZone);
        final var navigatedZones = navigateDangerZone(start, new LinkedHashSet<>());
        final var results = new ArrayList<>(navigatedZones);
        return DangerZone.of(results);
    }

    private Set<NavigableMapZone> navigateDangerZone(NavigableMapZone start, LinkedHashSet<NavigableMapZone> mapPoints) {
        if (start == null || start.center().isMaximum()) {
            return mapPoints;
        }
        if (mapPoints.contains(start)) {
            return mapPoints;
        }

        mapPoints.add(start);

        navigateDangerZone(start.right(), mapPoints);
        navigateDangerZone(start.below(), mapPoints);
        navigateDangerZone(start.left(), mapPoints);
        navigateDangerZone(start.above(), mapPoints);

        return mapPoints;
    }

}

import io.almac.adventofcode.v2021.utils.FileInputReader;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

public class UnderwaterRawDataReader {

    public UnderwaterMap readData() {
        final var fileInputReader = new FileInputReader();
        final var lines = fileInputReader.readLines("day9/input");
        final var converter = new MapPointConverter();
        final List<MapZone> mapZones = new LinkedList<>();

        Function<String, List<Integer>> transformation = this::stringToInteger;

        BiFunction<List<String>, Integer, String> previousRow = (rows, i) -> i - 1 >= 0 ? rows.get(i - 1) : null;
        BiFunction<List<String>, Integer, String> currentRow = List::get;
        BiFunction<List<String>, Integer, String> nextRow = (rows, i) -> i + 1 < rows.size() ? rows.get(i + 1) : null;

        for (int i = 0; i < lines.size(); i++) {
            var prev = previousRow.apply(lines, i);
            var curr = currentRow.apply(lines, i);
            var next = nextRow.apply(lines, i);

            final var mapPointRow = converter.toMapPoint(
                    transformation.apply(prev),
                    transformation.apply(curr),
                    transformation.apply(next),
                    i
            );

            mapZones.addAll(mapPointRow);
        }

        final Map<MapPoint, MapZone> map = new LinkedHashMap<>();
        mapZones.forEach(mapZone -> {
            map.put(mapZone.getCoordinates(), mapZone);
        });

        return UnderwaterMap.of(map);
    }

    private List<Integer> stringToInteger(String s) {
        if (s == null) {
            return null;
        }
        final var row = new LinkedList<Integer>();
        for (var i = 0; i < s.length(); i++) {
            row.add(Integer.valueOf(String.valueOf(s.charAt(i))));
        }
        return row;
    }
}

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

public class UnderwaterMapToNavigableMapConverter {

    public NavigableUnderwaterMap convert(UnderwaterMap underwaterMap) {
        final var map = underwaterMap.getNavigation();
        final var end = findMaximumXYCoordinates(map.keySet());

        final var lowResolutionMap = extractLowResolutionMap(map, end, NavigableUnderwaterMap.of(new LinkedHashMap<>()));
        return extractHighResolutionMap(lowResolutionMap, end);
    }

    private NavigableUnderwaterMap extractHighResolutionMap(NavigableUnderwaterMap res, MapPoint end) {
        final var resMap = res.getNavigableMapZoneMap();
        Function<MapPoint, NavigableMapZone> center = resMap::get;
        Function<MapPoint, NavigableMapZone> above = (point) -> point.getY() - 1 >= 0 ? resMap.get(MapPoint.of(point.getX(), point.getY() - 1)) : null;
        Function<MapPoint, NavigableMapZone> below = (point) -> point.getY() + 1 <= end.getY() ? resMap.get(MapPoint.of(point.getX(), point.getY() + 1)) : null;
        Function<MapPoint, NavigableMapZone> left = (point) -> point.getX() - 1 >= 0 ? resMap.get(MapPoint.of(point.getX() - 1, point.getY())) : null;
        Function<MapPoint, NavigableMapZone> right = (point) -> point.getX() + 1 <= end.getX() ? resMap.get(MapPoint.of(point.getX() + 1, point.getY())) : null;

        for (var j = 0; j <= end.getY(); j++) {
            for (var i = 0; i <= end.getX(); i++) {
                MapPoint currentPosition = MapPoint.of(i, j);
                final var navi = center.apply(currentPosition)
                        .above(above.apply(currentPosition))
                        .below(below.apply(currentPosition))
                        .left(left.apply(currentPosition))
                        .right(right.apply(currentPosition));

                res.getNavigableMapZoneMap().put(currentPosition, navi);
            }
        }

        return res;
    }

    private NavigableUnderwaterMap extractLowResolutionMap(Map<MapPoint, MapZone> map, MapPoint end, NavigableUnderwaterMap res) {
        for (var j = 0; j <= end.getY(); j++) {
            for (var i = 0; i <= end.getX(); i++) {
                MapPoint currentPosition = MapPoint.of(i, j);
                final var navi = NavigableMapZone.builder()
                        .center(map.get(currentPosition))
                        .build();
                res.getNavigableMapZoneMap().put(currentPosition, navi);
            }
        }
        return res;
    }

    public MapPoint findMaximumXYCoordinates(Set<MapPoint> mapPoints) {
        MapPoint maxMapPoint = MapPoint.of(0, 0);
        for (var mapPoint : mapPoints) {
            if (maxMapPoint.getX() < mapPoint.getX()) {
                maxMapPoint = MapPoint.of(mapPoint.getX(), maxMapPoint.getY());
            }
            if (maxMapPoint.getY() < mapPoint.getY()) {
                maxMapPoint = MapPoint.of(maxMapPoint.getX(), mapPoint.getY());
            }
        }
        return maxMapPoint;
    }

}

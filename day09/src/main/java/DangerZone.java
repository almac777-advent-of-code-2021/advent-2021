import lombok.Value;

import java.util.List;

@Value(staticConstructor = "of")
public class DangerZone {
    List<NavigableMapZone> basin;
}

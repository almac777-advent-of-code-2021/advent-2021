import java.util.List;

public class Day9 {
    public static void main(String[] args) {
        part1();
        part2();
    }

    private static void part1() {
        final var underwaterRawDataReader = new UnderwaterRawDataReader();
        final var res = underwaterRawDataReader.readData();
        System.out.println("Answer 1 > "  + res.calculateDangerPoints());
    }

    private static void part2() {
        final var underwaterRawDataReader = new UnderwaterRawDataReader();
        final var underwaterMap = underwaterRawDataReader.readData();
        final var converter = new UnderwaterMapToNavigableMapConverter();
        final var dangerPoints = underwaterMap.dangerPoints();
        final var navigableUnderwaterMap = converter.convert(underwaterMap);
        final var dangerZoneFinder = new DangerZoneFinder();
        final var dangerZones = dangerZoneFinder.findDangerZones(dangerPoints, navigableUnderwaterMap);
        final var zonesListedBySize = dangerZones.stream()
                .map(DangerZone::getBasin)
                .map(List::size)
                .sorted((x, y) -> Integer.compare(x, y) * -1)
                .toList();

        final var res = zonesListedBySize.get(0) * zonesListedBySize.get(1) * zonesListedBySize.get(2);

        System.out.println("Answer 2 > " + res);
    }

}

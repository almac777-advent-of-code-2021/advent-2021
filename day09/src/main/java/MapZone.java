import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class MapZone {

    int left;
    int center;
    int right;

    int above;
    int below;

    int diagonalUpLeft;
    int diagonalUpRight;
    int diagonalDownLeft;
    int diagonalDownRight;

    MapPoint coordinates;

    boolean checkIsDanger() {
        return center < left
                && center < right
                && center < above
                && center < below;
    }

    public boolean isMaximum() {
        return center == 9;
    }
}

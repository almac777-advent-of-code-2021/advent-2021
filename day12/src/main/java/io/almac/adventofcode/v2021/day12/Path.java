package io.almac.adventofcode.v2021.day12;

import lombok.Getter;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class Path {

    private final List<Node> path = new LinkedList<>();

    public void add(Node node) {
        path.add(node);
    }

    public boolean hasAlreadyVisited(final Node node) {
        return node.isSingleVisit() && path.stream().anyMatch(n -> n.getName().equals(node.getName()));
    }

    public boolean hasVisitedTwice(final Node node) {
        int counter = 0;
        for (final var n : path) {
            if (n.equals(node)) {
                counter++;
            }
        }
        if (counter > 2) {
            throw new RuntimeException();
        }
        return counter == 2;
    }

    @Override
    public String toString() {
        return path.stream()
                .map(Node::getName)
                .collect(Collectors.joining("-"));
    }

    public void backtrack(Node node) {
        for (var i = getPath().size() - 1; i >= 0; i--) {
            if (getPath().get(i).equals(node)) {
                getPath().remove(i);
                return;
            }
        }
    }

    public Path copy() {
        Path p = new Path();
        p.getPath().addAll(this.path);
        return p;
    }
}

package io.almac.adventofcode.v2021.day12;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Builder
@Getter
@ToString(onlyExplicitlyIncluded = true)
public class Node {

    @ToString.Include
    private final String name;

    private final boolean isMultiPass;
    private final boolean isStartNode;
    private final boolean isEndNode;
    private final List<Node> neighbors;

    public boolean isSingleVisit() {
        return !isMultiPass && !isStartNode && !isEndNode;
    }

}

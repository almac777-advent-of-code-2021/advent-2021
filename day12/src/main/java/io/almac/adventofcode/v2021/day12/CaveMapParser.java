package io.almac.adventofcode.v2021.day12;

import io.almac.adventofcode.v2021.utils.FileInputReader;
import io.almac.adventofcode.v2021.utils.InputReader;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class CaveMapParser {
    private final InputReader inputReader = new FileInputReader();

    private static final int FROM_INDEX = 0;
    private static final int TO_INDEX = 1;

    private static final String START_NODE_MARKER = "start";
    private static final String END_NODE_MARKER = "end";
    private static final String SEPARATOR = "-";

    public CaveMap readCaveMapFrom(String coordinates) {
        List<String> lines = inputReader.readLines(coordinates);
        Map<String, Node> nodeCache = new HashMap<>();
        for (final var line : lines) {
            final var fromTo = line.split(SEPARATOR);
            final var from = fromTo[FROM_INDEX];
            final var to = fromTo[TO_INDEX];

            nodeCache.putIfAbsent(from, Node.builder()
                    .name(from)
                    .isStartNode(from.equals(START_NODE_MARKER))
                    .isEndNode(from.equals(END_NODE_MARKER))
                    .isMultiPass(from.equals(from.toUpperCase()))
                    .neighbors(new LinkedList<>())
                    .build());
            nodeCache.putIfAbsent(to, Node.builder()
                    .name(to)
                    .isStartNode(to.equals(START_NODE_MARKER))
                    .isEndNode(to.equals(END_NODE_MARKER))
                    .isMultiPass(to.equals(to.toUpperCase()))
                    .neighbors(new LinkedList<>())
                    .build());

            final var s = nodeCache.get(from);
            final var e = nodeCache.get(to);

            s.getNeighbors().add(e);
            e.getNeighbors().add(s);
        }

        return CaveMap.builder()
                .startNode(nodeCache.values().stream().filter(Node::isStartNode).findFirst().orElseThrow())
                .endNode(nodeCache.values().stream().filter(Node::isEndNode).findFirst().orElseThrow())
                .nodes(nodeCache.values().stream().toList())
                .build();
    }

}

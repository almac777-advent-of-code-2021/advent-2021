package io.almac.adventofcode.v2021.day12;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

@Builder
@Getter
public class CaveMap {

    private final List<Node> nodes;
    private final Node startNode;
    private final Node endNode;

}

package io.almac.adventofcode.v2021.day12;

import java.util.LinkedHashSet;
import java.util.Set;

public class SimpleNavigator {

    public Set<String> retrievePaths(CaveMap cv, boolean isComplexModeEnabled) {
        final Set<String> res = new LinkedHashSet<>();
        if (isComplexModeEnabled) {
            cv.getNodes().stream().filter(Node::isSingleVisit).forEach(servesAsSpecialNode -> {
                Set<Path> paths = new LinkedHashSet<>();
                initiate(cv.getStartNode(), null, servesAsSpecialNode, new Path(), paths);
                res.addAll(paths.stream().map(Path::toString).toList());
            });
        } else {
            Set<Path> paths = new LinkedHashSet<>();
            initiate(cv.getStartNode(), null, null, new Path(), paths);
            res.addAll(paths.stream().map(Path::toString).toList());
        }
        return res;
    }

    private void initiate(Node currentNode, Node lastNode, Node nodeAllowedToBeVisitedTwice, Path currentPath, Set<Path> paths) {
        if (currentNode.equals(lastNode)) {
            return;
        }
        if (! currentNode.equals(nodeAllowedToBeVisitedTwice)) {
            if (currentPath.hasAlreadyVisited(currentNode)) {
                return;
            }
        } else {
            if (currentPath.hasVisitedTwice(currentNode)) {
                return;
            }
        }

        currentPath.add(currentNode);
        if (currentNode.isEndNode()) {
            paths.add(currentPath.copy());
        } else {
            for (final var node : currentNode.getNeighbors()) {
                // skip loops and start node
                if (node.isStartNode()
                        || node.equals(currentNode)) {
                    continue;
                }
                initiate(node, currentNode, nodeAllowedToBeVisitedTwice, currentPath, paths);
            }
        }
        currentPath.backtrack(currentNode);
    }

}

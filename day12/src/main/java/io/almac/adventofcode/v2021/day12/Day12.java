package io.almac.adventofcode.v2021.day12;

public class Day12 {
    public static void main(String[] args) {
        part1();
        part2();
    }

    private static void part1() {
        final var caveMap = new CaveMapParser().readCaveMapFrom("day12/input");
        final var paths = new SimpleNavigator().retrievePaths(caveMap, false);
        paths.forEach(System.out::println);
        System.out.println("Answer 1 > " + paths.size());
    }

    private static void part2() {
        final var caveMap = new CaveMapParser().readCaveMapFrom("day12/input");
        final var paths = new SimpleNavigator().retrievePaths(caveMap, true);
        paths.forEach(System.out::println);
        System.out.println("Answer 2 > " + paths.size());
    }
}

package io.almac.adventofcode.v2021.day12;

import io.almac.adventofcode.v2021.day12.CaveMapParser;
import io.almac.adventofcode.v2021.day12.Node;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CaveMapParserTest {

    @Test
    void parseCaveMapFromInput() {
        final var caveMapParser = new CaveMapParser();
        final var caveMap = caveMapParser.readCaveMapFrom("day12/smol-input");
        assertThat(caveMap.getNodes())
                .isNotNull()
                .isNotEmpty()
                .hasSize(6)
                .matches((n) -> n.stream().filter(Node::isStartNode).toList().size() == 1)
                .matches((n) -> n.stream().filter(Node::isEndNode).toList().size() == 1)
                .matches((n) -> n.stream().filter(Node::isMultiPass).toList().size() == 1);
    }

}

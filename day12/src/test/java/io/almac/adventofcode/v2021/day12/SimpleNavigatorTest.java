package io.almac.adventofcode.v2021.day12;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SimpleNavigatorTest {

    private CaveMapParser caveMapParser;

    @BeforeEach
    void setup() {
        caveMapParser = new CaveMapParser();
    }

    @Test
    void navigateEz() {
        final var caveMap = caveMapParser.readCaveMapFrom("day12/ez-input");
        final var paths = new SimpleNavigator().retrievePaths(caveMap, false);
        paths.forEach(System.out::println);
        assertThat(paths.size()).isEqualTo(3);
    }

    @Test
    void navigateNotSoEz() {
        final var caveMap = caveMapParser.readCaveMapFrom("day12/not-so-ez-input");
        final var paths = new SimpleNavigator().retrievePaths(caveMap, false);
        paths.forEach(System.out::println);
        assertThat(paths.size()).isEqualTo(10);
    }

    @Test
    void navigateSmol() {
        final var caveMap = caveMapParser.readCaveMapFrom("day12/smol-input");
        final var paths = new SimpleNavigator().retrievePaths(caveMap, false);
        paths.forEach(System.out::println);
        assertThat(paths.size()).isEqualTo(10);
    }

    @Test
    void navigateMedium() {
        final var caveMap = caveMapParser.readCaveMapFrom("day12/medium-input");
        final var paths = new SimpleNavigator().retrievePaths(caveMap, false);
        assertThat(paths.size()).isEqualTo(19);
    }

    @Test
    void navigateLarge() {
        final var caveMap = caveMapParser.readCaveMapFrom("day12/large-input");
        final var paths = new SimpleNavigator().retrievePaths(caveMap, false);
        assertThat(paths.size()).isEqualTo(226);
    }
}
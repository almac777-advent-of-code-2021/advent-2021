package io.almac.adventofcode.v2021.day12;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ComplexNavigationTest {

    @Test
    void testSmolInput() {
        final var caveMap = new CaveMapParser().readCaveMapFrom("day12/smol-input");
        final var paths = new SimpleNavigator().retrievePaths(caveMap, true);
        paths.forEach(System.out::println);
        assertThat(paths.size()).isEqualTo(36);
    }

    @Test
    void navigateMedium() {
        final var caveMap = new CaveMapParser().readCaveMapFrom("day12/medium-input");
        final var paths = new SimpleNavigator().retrievePaths(caveMap, true);
        assertThat(paths.size()).isEqualTo(103);
    }

    @Test
    void navigateLarge() {
        final var caveMap = new CaveMapParser().readCaveMapFrom("day12/large-input");
        final var paths = new SimpleNavigator().retrievePaths(caveMap, true);
        assertThat(paths.size()).isEqualTo(3509);
    }

}
